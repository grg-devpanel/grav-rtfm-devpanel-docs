---
title: Secrets
description: How to manage and share secrets
taxonomy:
    category:
        - docs
---

Secrets are named sets of key=value pairs that are stored in the devpanel secret manager and can be injected into cron jobs and deployments (which will be explained in the later articles) as environment variables.

Secrets are a convenient way to avoid the need to have plain text passwords in the source code, as well as other hard-coded parameters, such as parameters for connecting to a database: SQL server name, user name, database name and, as already mentioned, the password. Instead, the source code can use the environment variables which are set by injecting secrets.

If we have multiple secrets, each representing a set of parameters for connecting to different databases, then we can easily switch databases in our cron jobs or deployments by changing the name of the secret in use.

# Opening the secret manager
In the project navigation menu, click on "Secret Manager".

![secrets-01-secret-manager-click.png](secrets-01-secret-manager-click.png)

# Creating new secrets

Let's assume that we have two different databases, live and dev. We will create two secrets (sets of variables), database-live and database-dev, which will contains parameters for connecting to the two databases.

In the secret manager, click on "Add new variable set", type the secret name in the popup window and click "OK".

![secrets-02-add-new.png](secrets-02-add-new.png)

# Adding new variables to secrets

- Under the secret name "database-live", click on "Add new variable".
![secrets-03-add-new-variable-click.png](secrets-03-add-new-variable-click.png)
- Then specify the key (variable name) and the value in the popup window.
![secrets-04-add-new-variable-key-value.png](secrets-04-add-new-variable-key-value.png)
- Go ahead and create all the required database connection parameters using the "Add new variable" button multiple times:
![secrets-05-all-variables-created.png](secrets-05-all-variables-created.png)

# Create another secret

Similarly, create another secret, database-dev, with the same set of variables.

![secrets-06-another-secret-created.png](secrets-06-another-secret-created.png)

# Using secrets

Please see instructions on how to inject secrets into cron jobs and deployments in the respective articles:
- [Cron Jobs](/administration/cronjobs): Creating, editing and running scheduled operations.
- [Deployments](/deployments/creating-deployments): Managing Application and Kubernetes Deployments.
