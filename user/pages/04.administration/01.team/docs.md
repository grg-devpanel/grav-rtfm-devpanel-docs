---
title: Team Management
description: Creating and managing your teams
taxonomy:
    category:
        - docs
---

# Managing People and Security

There are three different ways you can manage people in DevPanel...
1. Collaborators
2. Teams
3. Users (requires org admin privs)

# Collaborators

The easiest and quickest way of inviting people into your DevPanel Workspace is by adding them as Collaborators.

## Add a Collaborator

To do this, go to the People tab in your workspace and click the "Add Collaborator" button. 

![2021-03-05_13-52-46_collab_add_0.png](2021-03-05_13-52-46_collab_add_0.png)

When adding a Collaborator, you can give them one of three roles:
- Workspace Admin/Owner: Can add/delete owners, admins & users + delete infrastructure
- Workspace Admin: Can add/delete admins & users + create & delete projects
- Workspace Users: Can create & delete applications in projects

## Invite by Email

To add a new Collaborator, follow the following steps
1. go to the People tab and click Add Collaborator button
2. enter their email address
3. select their role
4. click ok (check mark)
5. verify that the collaborator's email shows up

![2021-03-05_13-17-43_collab_add_2.png](2021-03-05_13-17-43_collab_add_2.png)

They should receive an email with a link to join DevPanel and access your workspace.

!!!! Users you invite can sign up with another email address if they like... for example, if you invite collaborator@gmail.com, they might accept your invite and join DevPanel (and your Workspace) as collaborator@somecompany.com.

!!!! Users who already have an account on DevPanel can accept your invite under their existing account - which may be different than the one you invited them at.

! If you want to accept a new invite under a new/different/separate DevPanel account, you must first log out of DevPanel before accepting the invite. You will then get a chance to login with your existing account to accept the invite or to create a new account for this invite.

!!!! Users you invite also have the option to DECLINE your invite.

## Invite by Link

There are times when the user you're inviting does not get the email, maybe the email goes into SPAM, or gets blocked for some reason, or there's a delay in getting the email... BUT you're on a meeting with your collaborators on Slack or Zoom and it's just easier for you to send them a join-now link over chat... 

For that, use the "Invite Link" function.

Next to each pending user, you'll see three "Action" buttons:
1. Copy Invite Link
2. Regenerate Invite Link
3. Delete (rescind/revoke/withdraw) Invitation

![2021-03-05_13-12-01_collab_invite.png](2021-03-05_13-12-01_collab_invite.png)

To get the Invite Link, use the following steps:
1. click on the copy invite link button
2. click on the "Copy new url to clip board" text in the pop-up

![2021-03-05_13-21-13_invite_2.png](2021-03-05_13-21-13_invite_2.png)

Now you can paste that link in an Email, Slack, or on Zoom to send it directly to the person you're inviting. They will be able to join your Workspace instantly with this link.

# Teams

You can group a bunch of people in a Team and assign them to your workspaces as Users or Admins.

As Users, the team members will not be able to invite others to the workspace and will not be able to delete resources.

The benefit of using Teams is that when you add a new user to a team, that new user is automatically added to ALL workspaces that the team has access to. Conversely, when you delete a user from a team, that user loses access to ALL workspaces that the team has access to. 

You can find Teams under People in your Workspace...

![2021-03-05_14-59-09_team_find.png](2021-03-05_14-59-09_team_find.png)

## Managing Teams

To manage Team members, 
1. Add a Team
2. Make sure the Team shows in the list
3. Click Teams in the top navigation

![2021-03-05_13-30-25_team.png](2021-03-05_13-30-25_team.png)

## Types of Teams

On the Teams pages, you can expect to find three different types of Teams...
1. Instance Admin List - only accessible to DevPanel Instance & Org Admins
2. User List - list of ALL users that you can manage
3. Team Name - each team you've created

![2021-03-05_13-31-55_team_0.png](2021-03-05_13-31-55_team_0.png)

You can only edit & delete (4) the teams that you've created... the other lists are managed by the system.

# Users

Under Teams, you can also manage your Users.

![2021-03-05_15-17-50_user_types_2.png](2021-03-05_15-17-50_user_types_2.png)

## User Types

There are three types of Users

1. Instance Owners - a super admin or org admin who can delete any/all resources
2. Instance Admins - full access, can create Workspaces, can invite Workspace admins, can delete any/all resources, but cannot remove owners
3. Members - have access within certain workspaces

## User Status

There are three types of User Status

4. Active - allowed to use the system
5. Inactive - default status if user signs up without an invite (ie: waiting to be approved)
6. Blocked - locked out from  using the system or any workspaces

# Security

Blocked users will not be able to login and will effectively lose access to all workspaces and projects in DevPanel. 

DevPanel Development and Production environment, cluster and server access is based on single sign-on so blocked users SHOULD lose access to all resouces managed by single sign-on. 

Blocked users, however, will remain on teams and part of workspaces... so if and when you activate them, they will regain their prior access.


! DevPanel, itself, does not rely on user's SSH keys for resource access so we don't manage any keys through the dashboard. However, this DOES NOT mean that a malicious actor could not leave behind SSH keys as backdoors on your infrastructure. It is your responsibility to secure and scan your infrastructure and keep your users and applications safe.

! Application access is generally managed in applications directly so blocking users in DevPanel will not block those users from accessing your applications directly. Blocking users on DevPanel should be part of a comprehensive plan for protecting your applications and infrastructure but never the only thing that you rely on.
