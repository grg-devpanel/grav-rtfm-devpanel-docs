---
title: Infrastructure
description: Infrastructure components and details
taxonomy:
    category:
        - docs
---

# Infrastructure Setup
Here is a simple diagram of the default infrastructure that DevPanel sets up in user's account:

## Default Infrastructure

Once it has access to your account, DevPanel uses Hashicorp's TerraForm to create infrastructure in your AWS account. 

[Learn more about TerraForm](https://www.terraform.io/)

![client-infra.jpg](client-infra.jpg)

Depending on your needs, this infra can (and often is) customized. See list of components below for optional components not shown on this diagram.

!!!! All your sites and applications run on top of this infrastruture. 

## Infrastructure Components
Here are links to the infrastructure components used on AWS:

[AWS Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc).
Every new cluster is contained in its own VPC

[Amazon Elastic Compute Cloud (Amazon EC2)](https://aws.amazon.com/ec2).
We can use On-Demand, Reserved, and SPOT instances of EC2 servers. During infrastructure creation, you can specify the node (server) size that you'd like us to use by default in your clusters. 

[Elastic Load Balancers (ALB & NLB)](https://aws.amazon.com/elasticloadbalancing).
You servers are behind AWS Managed Load Balancers. These load balancers will not only distribute load but also route traffic to available servers in case of failure with certain servers or entire data centers (AZ) in a region.

[NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html).
These enable instances in a private subnet to connect to the internet or other AWS services, but prevent the internet from initiating a connection with those instances.

[Amazon Elastic Kubernetes Service (Amazon EKS)](https://aws.amazon.com/eks).
EKS gives you the flexibility to start, run, and scale Kubernetes applications in the AWS cloud or on-premises. Amazon EKS helps you provide highly-available and secure clusters and automates key tasks such as patching, node provisioning, and updates.

[Amazon Elastic File System (Amazon EFS)](https://aws.amazon.com/efs/).
EFS provides a simple, serverless, set-and-forget, elastic file system that lets you share file data without provisioning or managing storage. It can be used with AWS Cloud services and on-premises resources, and is built to scale on demand to petabytes without disrupting applications. With Amazon EFS, you can grow and shrink your file systems automatically as you add and remove files, eliminating the need to provision and manage capacity to accommodate growth.

[Amazon Relational Database Service (Amazon RDS)](https://aws.amazon.com/rds/).
RDS makes it easy to set up, operate, and scale a relational database in the cloud. It provides cost-efficient and resizable capacity while automating time-consuming administration tasks such as hardware provisioning, database setup, patching and backups. It frees you to focus on your applications so you can give them the fast performance, high availability, security and compatibility they need.

[Amazon Simple Storage Service (Amazon S3)](https://aws.amazon.com/s3/).
S3 is an object storage service that offers industry-leading scalability, data availability, security, and performance. This means customers of all sizes and industries can use it to store and protect any amount of data for a range of use cases, such as data lakes, websites, mobile applications, backup and restore, archive, enterprise applications, IoT devices, and big data analytics. Amazon S3 provides easy-to-use management features so you can organize your data and configure finely-tuned access controls to meet your specific business, organizational, and compliance requirements. Amazon S3 is designed for 99.999999999% (11 9's) of durability, and stores data for millions of applications for companies all around the world

[Amazon Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/).
Amazon Elasticsearch Service is a fully managed service that makes it easy for you to deploy, secure, and run Elasticsearch cost effectively at scale. You can build, monitor, and troubleshoot your applications using the tools you love, at the scale you need. 

[Amazon ElastiCache](https://aws.amazon.com/elasticache/).
Amazon ElastiCache allows you to seamlessly set up, run, and scale popular open-source compatible in-memory data stores in the cloud. Build data-intensive apps or boost the performance of your existing databases by retrieving data from high throughput and low latency in-memory data stores. Amazon ElastiCache offers fully managed Redis and Memcached for your most demanding applications that require sub-millisecond response times.

[Internet gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html).
An internet gateway is a horizontally scaled, redundant, and highly available VPC component that allows communication between your VPC and the internet. An internet gateway serves two purposes: to provide a target in your VPC route tables for internet-routable traffic, and to perform network address translation (NAT) for instances that have been assigned public IPv4 addresses. For more information, see Enabling internet access.

[Varnish](https://aws.amazon.com/quickstart/architecture/varnish/) (not shown on diagram).
Varnish speeds up a website by caching (storing) a copy of a page served by your web server the first time a user visits your page. The next time the user requests the same page, the cache will serve the copy quickly, instead of requesting the page from the web server again.

[Amazon CloudFront](https://aws.amazon.com/cloudfront) (not shown on diagram).
Amazon CloudFront is a fast content delivery network (CDN) service that securely delivers data, videos, applications, and APIs to customers globally with low latency, high transfer speeds, all within a developer-friendly environment. CloudFront offers the most advanced security capabilities, including field level encryption and HTTPS support, seamlessly integrated with AWS Shield, AWS Web Application Firewall and Route 53 to protect against multiple types of attacks including network and application layer DDoS attacks. These services co-reside at edge networking locations – globally scaled and connected via the AWS network backbone – providing a more secure, performant, and available experience for your users.

[AWS Web Application Firewall (WAF)](https://aws.amazon.com/waf/) (not shown on diagram).
AWS WAF is a web application firewall that helps protect your web applications or APIs against common web exploits and bots that may affect availability, compromise security, or consume excessive resources. AWS WAF gives you control over how traffic reaches your applications by enabling you to create security rules that control bot traffic and block common attack patterns, such as SQL injection or cross-site scripting. You can also customize rules that filter out specific traffic patterns. You can get started quickly using Managed Rules for AWS WAF, a pre-configured set of rules managed by AWS or AWS Marketplace Sellers to address issues like the OWASP Top 10 security risks and automated bots that consume excess resources, skew metrics, or can cause downtime. These rules are regularly updated as new issues emerge. AWS WAF includes a full-featured API that you can use to automate the creation, deployment, and maintenance of security rules.

[AWS Shield](https://aws.amazon.com/shield) (not shown on diagram).
AWS Shield is a managed Distributed Denial of Service (DDoS) protection service that safeguards applications running on AWS. AWS Shield provides always-on detection and automatic inline mitigations that minimize application downtime and latency, so there is no need to engage AWS Support to benefit from DDoS protection. There are two tiers of AWS Shield - Standard and Advanced. All AWS customers benefit from the automatic protections of AWS Shield Standard, at no additional charge. AWS Shield Standard defends against most common, frequently occurring network and transport layer DDoS attacks that target your web site or applications. When you use AWS Shield Standard with Amazon CloudFront and Amazon Route 53, you receive comprehensive availability protection against all known infrastructure (Layer 3 and 4) attacks.

[Amazon CloudWatch](https://aws.amazon.com/cloudwatch/) (not shown on diagram).
Amazon CloudWatch is a monitoring and observability service built for DevOps engineers, developers, site reliability engineers (SREs), and IT managers. CloudWatch provides you with data and actionable insights to monitor your applications, respond to system-wide performance changes, optimize resource utilization, and get a unified view of operational health. CloudWatch collects monitoring and operational data in the form of logs, metrics, and events, providing you with a unified view of AWS resources, applications, and services that run on AWS and on-premises servers. You can use CloudWatch to detect anomalous behavior in your environments, set alarms, visualize logs and metrics side by side, take automated actions, troubleshoot issues, and discover insights to keep your applications running smoothly.
