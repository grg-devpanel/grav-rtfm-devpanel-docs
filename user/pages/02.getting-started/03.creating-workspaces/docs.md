---
title: Creating Workspaces
description: Creating a space for your apps and teams
taxonomy:
    category:
        - docs
---

# Creating Workspaces
Workspaces hold all your Projects, Applications and Teams.

Each Workspace is connected to at least one cluster.

Workspaces have Owners and Administrators too.

## Create a Workspace

![2021-02-24_16-38-31_create_a_workspace.png](2021-02-24_16-38-31_create_a_workspace.png)

## Name the Workspace

![2021-02-24_18-16-59_name_a_workspace.png](2021-02-24_18-16-59_name_a_workspace.png)

We recommend that you check "Protected Workspace" if you plan to run production workloads in this workspace and don't want someone to accidentally delete this workspace.

If you have an Enterprise Edition of DevPanel, then only the DevPanel administrators will be able to delete these workspaces after you create them.

## Link Workspace to AWS Account
When you create a workspace, you will be given an option to link that workspace to an AWS account. Just follow the steps below. There's a video that shows you how the process works... we recommend you watch the video the at least once before you attempt this process.

![2021-02-24_16-36-25_link_aws_account_to_workspace.png](2021-02-24_16-36-25_link_aws_account_to_workspace.png)

You can also link that workspace to a previously linked AWS account. For this, simply select another workspace (that you had created previously) from the dropdown menu and DevPanel will create infrastructure for this workspace in the account linked to the selected workspace.

![2021-02-24_18-18-02_link_workspace_to_existing_account.png](2021-02-24_18-18-02_link_workspace_to_existing_account.png)

## Invite Owners and Admins

Each Workspace can have multiple Owners and Admins. Owners can add and remove Admins, other Owners, and can delete the Workspace. Admins can add and remove other Admins and can manage Developers and Teams in the Workspace. Admins are also able to delete projects and applications at will.

You can also invite an entire Team as an Owner or Admin to a Workspace. The benefit of using teams is that when you add or remove people from a team, they will gain or lose their access across the board. 

![2021-02-24_18-18-57_invite_workspace_owners_and_admins.png](2021-02-24_18-18-57_invite_workspace_owners_and_admins.png)

Here's how it works:
1. If you select a team for access
1. you have to specify the level of access for that team
1. you can invite Owners by email (comma separated)
1. and invite Admins by email as well (comma separated)

## Specify and Size your Infrastructure

Next you can specify the region, the size, and specs for your cluster and your database.

![2021-02-24_18-22-25_workspace_infra_specs.png](2021-02-24_18-22-25_workspace_infra_specs.png)

You can click "Complete Setup" only after you agree to the conditions listed on the page. 

Your agreements include:
- Authorize DevPanel to configure and manage your account
- Authorize DevPanel to use managed services as needed
- Agree not to interfere with DevPanel's operations in this account
- Agree not to run anything else in this account besides DevPanel
- Understant that you are responsible for paying your cloud provider bills directly

!!!! The Infrastructure creation process can take up to 30 minutes.
