---
title: Requirements
description: Requirements for using the DevPanel platform
taxonomy:
    category:
        - docs
---

# NOTE: Free Demo Environments
If you want to just try DevPanel, please contact us and we will set up a Demo Environment for you and your team. 

The following setup process is for customers with paid accounts.

In order to start using the DevPanel platform, you will need to have the following:
# REQ: AWS account <!-- ![aws.png](/icons/aws.png) -->
Your own AWS account in which a an Elastic Kubernetes Cluster (EKS) will be created and managed by DevPanel. Other required resources will be also created, such as EC2 node groups (Kubernetes workload), Load Balancer, Elastic Relational Database (ERD), Elastic Block Store Volumes (EBS), and so on. 

! You will be responsible for paying the monthly bills from AWS. 

!!!! DevPanel uses US-East region by default. You will need access to that region from your account. To create infrastructure in other regions, contact support.

# REQ: GIT provider account <!-- ![git.png](/icons/git.png) -->
An account with one of the following git repository providers:
<!--
- [![github.png](/icons/github.png?cropResize=100,30)](https://github.com/)
- [![butbucket.png](/icons/butbucket.png?cropResize=100,30)](https://bitbucket.org/)
- [![gitlab.png](/icons/gitlab.png?cropResize=100,30)](https://gitlab.com/)
-->
- [GitHub](https://github.com/)
- [BitBucket](https://bitbucket.org/)
- [GitLab](https://gitlab.com/)

The source code of any projects (applications) you create will be stored in git repostitories under your account. Every project (application) must have a dedicated repository.

!!!! You will be able to use different git providers and different git accounts for different projects.

