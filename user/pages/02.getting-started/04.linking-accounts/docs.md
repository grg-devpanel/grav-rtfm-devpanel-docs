---
title: Linking Accounts
description: Link your DevPanel with other accounts.
taxonomy:
    category:
        - docs
---

# Link DevPanel to AWS and Git Providers
Before you can use DevPanel to create any apps or sites in your AWS account, you need to link your DevPanel account with your AWS account and your Git provider.

When you link your AWS account with DevPanel, DevPanel will access your AWS account via an IAM role. This is the AWS recommended way of providing cross-account access.

Since DevPanel sets up infrastructure in your AWS account and manages that account, it needs admin privileges to your account. 

Before you can link your AWS account to DevPanel, ensure that you, yourself, have root access or admin privileges to your AWS account.

There are multiple ways to link your AWS, however, the simplest way to do it is while creating a Workspace. 

## Create a Workspace

![2021-02-24_16-38-31_create_a_workspace.png](2021-02-24_16-38-31_create_a_workspace.png)
When you create a workspace, you will be given an option to link that workspace to an AWS account. 

You can then link that workspace to a previously linked AWS account or to another /different AWS account. 

## Link to your AWS Account
![2021-02-24_16-36-25_link_aws_account_to_workspace.png](2021-02-24_16-36-25_link_aws_account_to_workspace.png)

Follow the instructions to first Login to your AWS account and then run a CloudFormation Template. 

The workspace creation process then proceeds to create infrastructure in your account. 

## Link to your Git provider
Before you click the button to link DevPanel to your Git provider, make sure you're logged into the correct account OR that you're logged out of all your git provider accounts. 

When you link DevPanel to your Git provider, GitLab for example, you will be prompted to access permissions. 

![gitlab_2021-06-11_13-46-50.png](gitlab_2021-06-11_13-46-50.png)

Once you Authorize DevPanel for access to your account, it will not only be able to read your repos (to create applications) but will also be able to create repos in your account on your behalf. 

