---
title: Creating Accounts
description: Creating your accounts on DevPanel
taxonomy:
    category:
        - docs
---

# Creating a DevPanel account
Go to the URL provided to you by your account manager and sign up using the following screen.

## Sign up
![2021-02-24_14-36-18.png](2021-02-24_14-36-18.png)


Process:
1. Select "Sign Up"
1. use your GitHub account
1. sign up with your Google account
1. or sign up with your email address
1. click "SIGN UP"

! You will get an email with a confirmation link that you'll have to click to activate your account.

Once you've activated your account, you will see the workspaces that you have access to.

!!!! If you do not see any workspaces after logging in, contact your support team for access.
