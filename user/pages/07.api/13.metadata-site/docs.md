---
title: 'Get Site Metadata'
description: 'How to get Site Metadata'
taxonomy:
    category:
        - docs
---

# Get Site Metadata
The following sample script shows how you can get a site's metadata...

```
$ cat get_meta_site.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

## Response

```
{
  "_id": "60993039c25947001370c",
  "tags": [
    {
      "_id": "6093bed752409d00130c0",
      "workspaces": [
        "6093bd33c80e5b0013cf9"
      ],
      "name": "Demo Drupal 9",
      "createdAt": "2021-05-06T10:03:03.300Z",
      "updatedAt": "2021-05-06T10:03:03.300Z"
    }
  ],
  "name": "Demo Drupal 9_1",
  "description": "Demo Drupal 9",
  "sourceCodeToken": null,
  "repositoryName": "may10th_drupal_9",
  "repositoryProvider": "GITHUB",
  "projectType": {
    "_id": "5f5600c4a19ceb6ef3d48",
    "name": "Drupal 9",
    "key": "drupal9",
    "order": 3,
    "initialRepo": "https://github.com/drupal/drupal",
    "preferredStack": "5cf673531c9d440000382",
    "stacks": [
      "5cf673531c9d440000382",
      "5d830bccd3201d97f6bcd",
      "5d83176ed3201d97f6be9"
    ]
  },
  "repositoryOwner": "johnnydevpanel",
  "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "workspace": "60992fddc25947001370c",
  "friendlyName": "wscyl",
  "repositoryId": "366046",
  "initialized": true,
  "createdAt": "2021-05-10T13:08:09.951Z",
  "updatedAt": "2021-05-12T16:21:57.587Z",
  "lastActivity": {
    "_id": "609c00a359ae0000181be",
    "action": "DEPLOY_APPLICATION",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "60993039c25947001370c",
    "application": "60993076fff9de0013e8d",
    "environment": "5f68e1b2fb6a1a001c758",
    "status": "SUCCESS",
    "createdAt": "2021-05-12T16:21:55.783Z",
    "updatedAt": "2021-05-12T16:23:18.479Z",
    "lastResponse": {
      "id": "609c00a359ae0000181be",
      "status": "SUCCESS",
      "payload": {},
      "task": "DEPLOY_APPLICATION"
    },
    "taskName": null
  },
  "repositoryWebHookId": "296599",
  "_workspace": {
    "_id": "60992fddc25947001370c",
    "tags": [
      {
        "_id": "5ec4069756b3d30014724",
        "workspaces": [
          "5f68da685dd7810e250db",
          "60752d7a29ca1c0016442",
          "6093bd33c80e5b0013cf9",
          "6094c75bc25947001370c",
          "60992fddc25947001370c",
          "609bfa99e265000013f0f"
        ],
        "name": "demo",
        "createdAt": "2020-05-19T16:17:27.113Z",
        "updatedAt": "2021-05-12T15:56:09.400Z"
      }
    ],
    "isProtected": true,
    "isTesting": false,
    "status": "ACTIVE",
    "name": "API Demo",
    "description": "API Demo",
    "accountType": "5f68da685dd7810e250db",
    "teams": [],
    "owner": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "namespace": "devpanel-1620651997078jykm",
    "tenant": "api-1620640441-org",
    "environment": "5f68e1b2fb6a1a001c758",
    "collaborators": [],
    "createdAt": "2021-05-10T13:06:37.842Z",
    "updatedAt": "2021-05-11T15:12:06.211Z",
    "logo": "https://files.site.devpanel.com/file-1620745921-logo.png",
    "ownerInfo": {
      "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
      "email_verified": "true",
      "email": "user@example.com",
      "role": "INSTANCE_ADMIN",
      "status": "ACTIVE",
      "user_metadata": {
        "githubAccessToken": "TQOIWRwwIuUGeIP"
      }
    }
  }
}

```
