---
title: 'Create Site'
description: 'Create Site'
taxonomy:
    category:
        - docs
---

# Create Site
The following sample script shows how you can create a site using an API.

```
$ cat create_site_in_demo_wp.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

# please create on git accesstoken to continue to use this api
# https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token
# *repositoryName* is the new repo's name which will be created
curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370c/sites' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
    "gitToken":"gho_nTQOIWRwwIuUGeI",
    "name":"Demo Drupal 9",
    "description":"Demo Drupal 9",
    "tags":[{"name":"Demo Drupal 9"}],
    "sourceCodeToken":null,
    "repositoryType":"NEW",
    "repositoryName":"may12th_drupal_9v2",
    "repositoryProvider":"GITHUB",
    "projectType":"drupal9",
    "repositoryOwner":null,
    "isPrivateRepository":false
}' | jq
```

## Response

```
{
  "tags": [
    "6093bed752409d00130c0"
  ],
  "_id": "609bfb67e265000013f0f",
  "name": "Demo Drupal 9",
  "description": "Demo Drupal 9",
  "sourceCodeToken": null,
  "repositoryName": "may12th_drupal_9v2",
  "repositoryProvider": "GITHUB",
  "projectType": "5f5600c4a19ceb6ef3d48",
  "repositoryOwner": "johnnydevpanel",
  "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "workspace": "60992fddc25947001370c",
  "friendlyName": "lewid",
  "repositoryId": "366772",
  "initialized": false,
  "createdAt": "2021-05-12T15:59:35.499Z",
  "updatedAt": "2021-05-12T15:59:39.696Z",
  "lastActivity": {
    "_id": "609bfb68e265000013f0f",
    "action": "INITIALIZE_REPOSITORY",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "609bfb67e265000013f0f",
    "status": "PENDING",
    "createdAt": "2021-05-12T15:59:36.958Z",
    "updatedAt": "2021-05-12T15:59:36.958Z"
  },
  "repositoryWebHookId": "296969"
}

```

