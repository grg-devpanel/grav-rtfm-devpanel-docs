---
title: 'Add People into Workspace'
description: 'Add People into Workspace'
taxonomy:
    category:
        - docs
---

# Add People to Workspaces
The following sample script shows how you can add people to Workspaces using an API.

```
$ cat add_member_into_wp.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370c/people' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
    "email":"user@example.com",
    "role":"OWNER"
  }' | jq
  
  ```
  
## Response

```
{
  "_id": "609c071d59ae0000181be",
  "email": "user@example.com",
  "role": "OWNER",
  "workspace": "60992fddc25947001370c",
  "invitedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "inviteType": "WORKSPACE_COLLABORATOR",
  "inviteStatus": "PENDING",
  "createdAt": "2021-05-12T16:49:33.306Z",
  "updatedAt": "2021-05-12T16:49:33.306Z"
}
```
