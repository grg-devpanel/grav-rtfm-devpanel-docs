---
title: 'List Workspace API'
description: 'List Workspaces you have access to...'
taxonomy:
    category:
        - docs
---

# List Workspaces API
Here's how you get a list of Workspaces you have access to...

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.

```
$ cat list_workspaces.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"

curl -s -X GET \
  https://api.site.devpanel.com/dev-v1/workspaces \
  -H "$AUTH_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H 'tenant: api-1620640441-org' | jq 'map({ "_id": ._id, "name": .name })'
  ```
Running this script will give you this output:

```
$ ./list_workspaces.sh
[
  {
    "_id": "60992fddc25947001370c",
    "name": "API Demo"
  }
]
```

Without `'map({ "_id": ._id, "name": .name })'`

```
[
  {
    "_id": "60992fddc25947001370ca04",
    "tags": [
      {
        "_id": "5ec4069756b3d30014724dff",
        "workspaces": [
          "5f68da685dd7810e250dbcbc",
          "60752d7a29ca1c00164421f6",
          "6093bd33c80e5b0013cf94be",
          "6094c75bc25947001370c9da",
          "60992fddc25947001370ca04",
          "609bfa99e265000013f0fd87"
        ],
        "name": "demo",
        "createdAt": "2020-05-19T16:17:27.113Z",
        "updatedAt": "2021-05-12T15:56:09.400Z"
      }
    ],
    "isProtected": true,
    "isTesting": false,
    "status": "ACTIVE",
    "name": "API Demo",
    "description": "API Demo",
    "accountType": "5f68da685dd7810e250dbcbc",
    "teams": [],
    "owner": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
    "namespace": "devpanel-1620651997078jykmxih",
    "tenant": "api-1620640441141-org",
    "environment": "5f68e1b2fb6a1a001c758684",
    "collaborators": [
      {
        "_id": "609c2bf459ae0000181be310",
        "role": "ADMIN",
        "isOwner": true,
        "collaborator": "9a46f134-708f-43f5-b7e6-238c7177d700"
      },
      {
        "_id": "609c3410e265000013f0fdaf",
        "role": "ADMIN",
        "isOwner": true,
        "collaborator": "5d4793af-b134-4f71-ba16-9a4e9a31e61b"
      }
    ],
    "createdAt": "2021-05-10T13:06:37.842Z",
    "updatedAt": "2021-05-12T20:01:20.436Z",
    "logo": "https://files.site.devpanel.com/file-1620745921750-logo.png",
    "ownerInfo": {
      "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
      "email_verified": "true",
      "email": "huytran+2@webenabled.com",
      "role": "INSTANCE_ADMIN",
      "status": "ACTIVE",
      "user_metadata": {
        "githubAccessToken": "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"
      }
    }
  },
  {
    "_id": "609bfa99e265000013f0fd87",
    "tags": [
      {
        "_id": "5ec4069756b3d30014724dff",
        "workspaces": [
          "5f68da685dd7810e250dbcbc",
          "60752d7a29ca1c00164421f6",
          "6093bd33c80e5b0013cf94be",
          "6094c75bc25947001370c9da",
          "60992fddc25947001370ca04",
          "609bfa99e265000013f0fd87"
        ],
        "name": "demo",
        "createdAt": "2020-05-19T16:17:27.113Z",
        "updatedAt": "2021-05-12T15:56:09.400Z"
      }
    ],
    "isProtected": true,
    "isTesting": false,
    "status": "ACTIVE",
    "name": "API Demo 2",
    "description": "API Demo 2",
    "accountType": "5f68da685dd7810e250dbcbc",
    "teams": [],
    "owner": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
    "namespace": "devpanel-1620834968632eujsixvv",
    "tenant": "api-1620640441141-org",
    "environment": "5f68e1b2fb6a1a001c758684",
    "collaborators": [],
    "createdAt": "2021-05-12T15:56:09.229Z",
    "updatedAt": "2021-05-12T15:56:44.591Z",
    "ownerInfo": {
      "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
      "email_verified": "true",
      "email": "huytran+2@webenabled.com",
      "role": "INSTANCE_ADMIN",
      "status": "ACTIVE",
      "user_metadata": {
        "githubAccessToken": "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"
      }
    }
  }
]
```

Inside each Workspace, there will be sites, branches, and applications.
