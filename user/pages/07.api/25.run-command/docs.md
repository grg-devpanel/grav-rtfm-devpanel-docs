---
title: 'Run Drush commands'
description: 'Run drush commands in branch container'
taxonomy:
    category:
        - docs
---

# Run "drush" commands 
The following sample script shows how you can run `drush` commands in branch container...

```
$ cat run_command_in_a_running_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/commands' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{ "commands": ["drush", "status"] }' | jq -r ".result"
```

## Response
```
 Drupal version         :  9.1.7                                                
 Site URI               :  http://default                                       
 Database driver        :  mysql                                                
 Database hostname      :  devpanel-cluster-vvybh.cso9f6tlh.us-west-2.rds.amazonaws.com                                       
 Database port          :                                                       
 Database username      :  useroqnj                                          
 Database name          :  nameoqnj                                          
 Database               :  Connected                                            
 Drupal bootstrap       :  Successful                                           
 Drupal user            :                                                       
 Default theme          :  bartik                                               
 Administration theme   :  seven                                                
 PHP executable         :  /usr/local/bin/php                                   
 PHP configuration      :                                                       
 PHP OS                 :  Linux                                                
 Drush script           :  /home/www/.composer/vendor/drush/drush/drush.php     
 Drush version          :  8.4.8                                                
 Drush temp directory   :  /tmp                                                 
 Drush configuration    :                                                       
 Drush alias files      :                                                       
 Install profile        :  standard                                             
 Drupal root            :  /var/www/html/web                                    
 Drupal Settings File   :  sites/default/settings.php                           
 Site path              :  sites/default                                        
 File directory path    :  sites/default/files                                  
 Sync config path       :  sites/default/files/config_LDffgk-brK4SAoRoc7UC_QPMfg4LNBi6_8_7hOSuhPT_UHmM7d-IYwiMQlu_u9m0s/sync                                                   
```

