---
title: 'List Backup Files of Branch'
description: 'List Backup Files of Branch'
taxonomy:
    category:
        - docs
---

# List Backup Files of a Branch
Here's how you can see the list of backup files of a branch...

We will show you how you can get a list of Database backups AND list of Static File backups.

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.

## Databases

```
$ cat list_backup_databases.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/databases' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

```
[
  {
    "_id": "609bfe0f59ae0000181be",
    "path": "https://devpanel-cluster-vvybh.s3.amazonaws.com/master-wscyl/dump/thkbnq7yjdc9h.tgz?AWSAccessKeyId=ASIAUJYT34E3R4UTH&Signature=2JA0irw7OPCaxEQOrs27sH4xcPc&x-amz-security-token=FwoGZXIvYXdzEEoaDEPcAUR%",
    "fileType": "EXPORT_DATABASE",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "location": "6099303bc25947001370c",
    "bucket": "presigned-url",
    "key": "609bfdf5e265000013f0f",
    "createdAt": "2021-05-12T16:10:55.458Z",
    "updatedAt": "2021-05-12T16:10:55.458Z"
  }
]
```

## Static Files

```
$ cat list_backup_files.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/files' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
 ```

```
[
  {
    "_id": "609bfe501472da05ea19d",
    "path": "https://devpanel-cluster-vvybh.s3.amazonaws.com/master-wscyl/dump/h7zytg4u9ewpa.tgz?AWSAccessKeyId=ASIAUJYT34E3Q3EL4&Signature=s4geMTfC7tz5KLg6teRCxJdl0uM%3-amz-security-token=FwoGZXIvYXdzEEoaDHqMUmoIUWJaxl0eJyK9AWUnm5fDyno3aYGNVUJY3x4dOryz6V0IHExx1K2%",
    "fileType": "EXPORT_FILE",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "location": "6099303bc25947001370c",
    "bucket": "presigned-url",
    "key": "609bfe34e265000013f0f",
    "createdAt": "2021-05-12T16:12:00.778Z",
    "updatedAt": "2021-05-12T16:12:00.778Z"
  }
]
```
