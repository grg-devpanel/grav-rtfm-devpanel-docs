---
title: 'Get PHP Log of Branch'
description: 'How to get PHP Log of a branch'
taxonomy:
    category:
        - docs
---

# Get PHP Log of Branch
The following sample script shows how you can get a branch's PHP Log file...

```
$ cat get_branch_log.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/logs' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq ".data"
  
```

## Response

```
[
  "2021-05-11T04:24:25.813088064Z [11-May-2021 04:24:25] NOTICE: fpm is running, pid 17",
  "2021-05-11T04:24:25.815768135Z [11-May-2021 04:24:25] NOTICE: ready to handle connections",
  "2021-05-11T04:24:26.319548404Z [2021-05-11T04:24:26.302Z] info  Wrote default config file to ~/.config/code-server/config.yaml",
  "2021-05-11T04:24:26.831926186Z [2021-05-11T04:24:26.829Z] info  code-server 3.9.1 e0203f2a36c9b7036fefa50eec6cf8fa36c5c",
  "2021-05-11T04:24:26.833050067Z [2021-05-11T04:24:26.832Z] info  Using user-data-dir ~/.local/share/code-server",
  "2021-05-11T04:24:26.854965000Z [2021-05-11T04:24:26.854Z] info  Using config file ~/.config/code-server/config.yaml",
  "2021-05-11T04:24:26.855150453Z [2021-05-11T04:24:26.854Z] info  HTTP server listening on http://0.0.0.0:8080 ",
  "2021-05-11T04:24:26.855312747Z [2021-05-11T04:24:26.854Z] info    - Authentication is disabled ",
  "2021-05-11T04:24:26.855613082Z [2021-05-11T04:24:26.855Z] info    - Not serving HTTPS ",
  "2021-05-11T04:30:15.287213258Z 127.0.0.1 -  11/May/2021:04:30:12 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.345123770Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.387031267Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.607849797Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.679876701Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.790983468Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.794879721Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.797450380Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.806981029Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:02.811474403Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
  "2021-05-11T06:24:03.064397286Z 127.0.0.1 -  11/May/2021:06:24:03 +0000 \"GET /index.php\" 200",
  "2021-05-11T09:39:06.005325612Z 127.0.0.1 -  11/May/2021:09:39:05 +0000 \"GET /index.php\" 200"
]
```

## Without map

Here's the output that you would get without `jq ".data"`

```
{
  "podNamesList": [
    {
      "name": "master-wscyl-54c7979-pcntl",
      "resourceVersion": "78859",
      "metadata": {
        "annotations": {
          "kubernetes.io/psp": "eks.privileged"
        },
        "creationTimestamp": "2021-05-11T04:24:14.000Z",
        "generateName": "master-wscyl-54c7979-",
        "labels": {
          "app.kubernetes.io/instance": "master-wscyl",
          "app.kubernetes.io/name": "lemp",
          "pod-template-hash": "54c7979"
        },
        "managedFields": [
          {
            "apiVersion": "v1",
            "fieldsType": "FieldsV1",
            "fieldsV1": {
              "f:metadata": {
                "f:generateName": {},
                "f:labels": {
                  ".": {},
                  "f:app.kubernetes.io/instance": {},
                  "f:app.kubernetes.io/name": {},
                  "f:pod-template-hash": {}
                },
                "f:ownerReferences": {
                  ".": {},
                  "k:{\"uid\":\"6d3c000a-3018-4fc5-8267-0c4a0c7b2\"}": {
                    ".": {},
                    "f:apiVersion": {},
                    "f:blockOwnerDeletion": {},
                    "f:controller": {},
                    "f:kind": {},
                    "f:name": {},
                    "f:uid": {}
                  }
                }
              },
              "f:spec": {
                "f:containers": {
                  "k:{\"name\":\"nginx\"}": {
                    ".": {},
                    "f:env": {
                      ".": {},
                      "k:{\"name\":\"NGINX_BACKEND_HOST\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_SERVER_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_SET_REAL_IP_FROM\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_VHOST_PRESET\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      }
                    },
                    "f:image": {},
                    "f:imagePullPolicy": {},
                    "f:name": {},
                    "f:ports": {
                      ".": {},
                      "k:{\"containerPort\":80,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:name": {},
                        "f:protocol": {}
                      }
                    },
                    "f:resources": {
                      ".": {},
                      "f:limits": {
                        ".": {},
                        "f:ephemeral-storage": {}
                      },
                      "f:requests": {
                        ".": {},
                        "f:cpu": {},
                        "f:ephemeral-storage": {},
                        "f:memory": {}
                      }
                    },
                    "f:terminationMessagePath": {},
                    "f:terminationMessagePolicy": {},
                    "f:volumeMounts": {
                      ".": {},
                      "k:{\"mountPath\":\"/var/www/html\"}": {
                        ".": {},
                        "f:mountPath": {},
                        "f:name": {}
                      }
                    }
                  },
                  "k:{\"name\":\"php\"}": {
                    ".": {},
                    "f:env": {
                      ".": {},
                      "k:{\"name\":\"APP_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"CODES_ENABLE\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"DB_HOST\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_NAME\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_PASSWORD\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_USER\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DEVPANEL_HOSTNAME\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"WEB_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      }
                    },
                    "f:image": {},
                    "f:imagePullPolicy": {},
                    "f:name": {},
                    "f:ports": {
                      ".": {},
                      "k:{\"containerPort\":8080,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:name": {},
                        "f:protocol": {}
                      },
                      "k:{\"containerPort\":9000,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:protocol": {}
                      }
                    },
                    "f:resources": {
                      ".": {},
                      "f:limits": {
                        ".": {},
                        "f:ephemeral-storage": {}
                      },
                      "f:requests": {
                        ".": {},
                        "f:cpu": {},
                        "f:ephemeral-storage": {},
                        "f:memory": {}
                      }
                    },
                    "f:terminationMessagePath": {},
                    "f:terminationMessagePolicy": {},
                    "f:volumeMounts": {
                      ".": {},
                      "k:{\"mountPath\":\"/var/www/html\"}": {
                        ".": {},
                        "f:mountPath": {},
                        "f:name": {}
                      }
                    }
                  }
                },
                "f:dnsPolicy": {},
                "f:enableServiceLinks": {},
                "f:nodeSelector": {
                  ".": {},
                  "f:groupType": {}
                },
                "f:restartPolicy": {},
                "f:schedulerName": {},
                "f:securityContext": {},
                "f:serviceAccount": {},
                "f:serviceAccountName": {},
                "f:terminationGracePeriodSeconds": {},
                "f:volumes": {
                  ".": {},
                  "k:{\"name\":\"app-volume\"}": {
                    ".": {},
                    "f:name": {},
                    "f:persistentVolumeClaim": {
                      ".": {},
                      "f:claimName": {}
                    }
                  }
                }
              }
            },
            "manager": "kube-controller-manager",
            "operation": "Update",
            "time": "2021-05-11T04:24:14.000Z"
          },
          {
            "apiVersion": "v1",
            "fieldsType": "FieldsV1",
            "fieldsV1": {
              "f:status": {
                "f:conditions": {
                  "k:{\"type\":\"ContainersReady\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  },
                  "k:{\"type\":\"Initialized\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  },
                  "k:{\"type\":\"Ready\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  }
                },
                "f:containerStatuses": {},
                "f:hostIP": {},
                "f:phase": {},
                "f:podIP": {},
                "f:podIPs": {
                  ".": {},
                  "k:{\"ip\":\"10.0.7.158\"}": {
                    ".": {},
                    "f:ip": {}
                  }
                },
                "f:startTime": {}
              }
            },
            "manager": "kubelet",
            "operation": "Update",
            "time": "2021-05-11T04:24:27.000Z"
          }
        ],
        "name": "master-wscyl-54c7979-pcntl",
        "namespace": "devpanel-1620651997078jykm",
        "ownerReferences": [
          {
            "apiVersion": "apps/v1",
            "blockOwnerDeletion": true,
            "controller": true,
            "kind": "ReplicaSet",
            "name": "master-wscyl-54c7979",
            "uid": "6d3c000a-3018-4fc5-8267-0c4a0c7b2"
          }
        ],
        "resourceVersion": "78859",
        "selfLink": "/api/v1/namespaces/devpanel-1620651997078jykm/pods/master-wscylanm-54c7979-pcntl",
        "uid": "434ed142-2108-426a-8d9b-8b63d4723"
      }
    }
  ],
  "data": [
    "2021-05-11T04:24:25.813088064Z [11-May-2021 04:24:25] NOTICE: fpm is running, pid 17",
    "2021-05-11T04:24:25.815768135Z [11-May-2021 04:24:25] NOTICE: ready to handle connections",
    "2021-05-11T04:24:26.319548404Z [2021-05-11T04:24:26.302Z] info  Wrote default config file to ~/.config/code-server/config.yaml",
    "2021-05-11T04:24:26.831926186Z [2021-05-11T04:24:26.829Z] info  code-server 3.9.1 e0203f2a36c9b7036fefa50eec6cf8fa36c5c",
    "2021-05-11T04:24:26.833050067Z [2021-05-11T04:24:26.832Z] info  Using user-data-dir ~/.local/share/code-server",
    "2021-05-11T04:24:26.854965000Z [2021-05-11T04:24:26.854Z] info  Using config file ~/.config/code-server/config.yaml",
    "2021-05-11T04:24:26.855150453Z [2021-05-11T04:24:26.854Z] info  HTTP server listening on http://0.0.0.0:8080 ",
    "2021-05-11T04:24:26.855312747Z [2021-05-11T04:24:26.854Z] info    - Authentication is disabled ",
    "2021-05-11T04:24:26.855613082Z [2021-05-11T04:24:26.855Z] info    - Not serving HTTPS ",
    "2021-05-11T04:30:15.287213258Z 127.0.0.1 -  11/May/2021:04:30:12 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.345123770Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.387031267Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.607849797Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.679876701Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.790983468Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.794879721Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.797450380Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.806981029Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:02.811474403Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200",
    "2021-05-11T06:24:03.064397286Z 127.0.0.1 -  11/May/2021:06:24:03 +0000 \"GET /index.php\" 200",
    "2021-05-11T09:39:06.005325612Z 127.0.0.1 -  11/May/2021:09:39:05 +0000 \"GET /index.php\" 200"
  ],
  "events": [
    {
      "message": "2021-05-11T04:24:25.813088064Z [11-May-2021 04:24:25] NOTICE: fpm is running, pid 17"
    },
    {
      "message": "2021-05-11T04:24:25.815768135Z [11-May-2021 04:24:25] NOTICE: ready to handle connections"
    },
    {
      "message": "2021-05-11T04:24:26.319548404Z [2021-05-11T04:24:26.302Z] info  Wrote default config file to ~/.config/code-server/config.yaml"
    },
    {
      "message": "2021-05-11T04:24:26.831926186Z [2021-05-11T04:24:26.829Z] info  code-server 3.9.1 e0203f2a36c9b7036fefa50eec6cf8fa36c5c"
    },
    {
      "message": "2021-05-11T04:24:26.833050067Z [2021-05-11T04:24:26.832Z] info  Using user-data-dir ~/.local/share/code-server"
    },
    {
      "message": "2021-05-11T04:24:26.854965000Z [2021-05-11T04:24:26.854Z] info  Using config file ~/.config/code-server/config.yaml"
    },
    {
      "message": "2021-05-11T04:24:26.855150453Z [2021-05-11T04:24:26.854Z] info  HTTP server listening on http://0.0.0.0:8080 "
    },
    {
      "message": "2021-05-11T04:24:26.855312747Z [2021-05-11T04:24:26.854Z] info    - Authentication is disabled "
    },
    {
      "message": "2021-05-11T04:24:26.855613082Z [2021-05-11T04:24:26.855Z] info    - Not serving HTTPS "
    },
    {
      "message": "2021-05-11T04:30:15.287213258Z 127.0.0.1 -  11/May/2021:04:30:12 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.345123770Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.387031267Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.607849797Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.679876701Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.790983468Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.794879721Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.797450380Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.806981029Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:02.811474403Z 127.0.0.1 -  11/May/2021:06:24:02 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T06:24:03.064397286Z 127.0.0.1 -  11/May/2021:06:24:03 +0000 \"GET /index.php\" 200"
    },
    {
      "message": "2021-05-11T09:39:06.005325612Z 127.0.0.1 -  11/May/2021:09:39:05 +0000 \"GET /index.php\" 200"
    }
  ],
  "sessionId": "609c08db59ae0000181be"
}

```
