---
title: 'Update Branch Config'
description: null
taxonomy:
    category:
        - docs
---

# Update Branch Config

`cat upgrade_branch.sh`

```
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441141-org"

curl -sX PUT \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370ca0a/branches/6099303bc25947001370ca0d' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441141-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{}' | jq
 ```

## Response  

 ```
 {
  "isEnableEditor": true,
  "isEnablePMA": true,
  "lock": false,
  "doNotIndex": null,
  "secureConnection": false,
  "_id": "6099303bc25947001370ca0d",
  "project": "60993039c25947001370ca0a",
  "name": "master",
  "branchType": "MASTER",
  "originBranch": "master",
  "createdAt": "2021-05-10T13:08:11.211Z",
  "updatedAt": "2021-05-12T16:52:52.876Z",
  "appSettings": {
    "type": "drupal9",
    "adminUser": "devpanel",
    "adminPassword": "pwd2oqnjnxkoqnjnxk"
  },
  "applicationName": "master-wscylanm",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "backupInterval": null,
  "copyDatabaseFilesApplication": null,
  "copyDatabaseFilesType": null,
  "copyDatabaseUrl": null,
  "copyFilesUrl": null,
  "customDomain": null,
  "dbName": "nameoqnjnxk",
  "dbPassword": "pwdoqnjnxk",
  "dbUser": "useroqnjnxk",
  "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
  "deploymentConfig": {
    "application": {
      "storage": "20Gi"
    },
    "images": {
      "nginx": {
        "env": [
          {
            "name": "NGINX_SERVER_ROOT",
            "value": "/var/www/html/web"
          },
          {
            "name": "NGINX_VHOST_PRESET",
            "value": "drupal9"
          },
          {
            "name": "NGINX_BACKEND_HOST",
            "value": "0.0.0.0"
          },
          {
            "name": "NGINX_SET_REAL_IP_FROM",
            "value": "10.0.0.0/16"
          }
        ]
      },
      "php": {
        "env": [
          {
            "name": "APP_ROOT",
            "value": "/var/www/html/"
          },
          {
            "name": "WEB_ROOT",
            "value": "/var/www/html/web"
          }
        ]
      }
    }
  },
  "editorHostname": "cs-master-wscylanm.app.devpanel.com",
  "editorPwd": "devpanel",
  "environment": "5f68e1b2fb6a1a001c758684",
  "fromApplicationName": "undefined-wscylanm",
  "fromDB": null,
  "hostname": "dev-1370ca04-1370ca0a-1370ca0d.app.devpanel.com",
  "isEnablePolymorphing": false,
  "isEnablePolyscripting": false,
  "lockPassword": null,
  "lockUsername": null,
  "namespace": "devpanel-1620651997078jykmxih",
  "oldHostname": null,
  "pmaHostname": "pma-master-wscylanm.app.devpanel.com",
  "replicas": 1,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "lastActivity": "609c07e3e265000013f0fda5",
  "status": "DEPLOY_APPLICATION_SUCCESS"
}
```
