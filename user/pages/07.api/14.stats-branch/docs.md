---
title: 'Get Statistics of a Branch'
description: 'Get Statistics of a Branch'
taxonomy:
    category:
        - docs
---

# Get Statistics of a Branch


! This API is still under development but might be available for preview. Please contact us for details.

The following sample script shows how you can get a branch's Traffic Stats.

```
$ cat get_branch_statistic.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/609bfb67e265000013f0fd8e/branches/60a384f6bfa6050013af3603/statistics?month=2021-05' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
  
```

## Response

```
{
  "Origin Hits": 51,
  "CDN Requests": 102,
  "startDate": "2021-05-01 00:00:00",
  "endDate": "2021-05-31 23:59:59"
}

```
