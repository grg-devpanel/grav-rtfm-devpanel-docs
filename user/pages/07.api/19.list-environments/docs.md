---
title: 'List Environments API'
description: 'List the Clusters/Infrastructure that you have access to...'
taxonomy:
    category:
        - docs
---

# List Environments API
Here's how you can get a list of Clusters and Infrastructure that you have access to...

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.


```
$ cat list_environments.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"

curl -s -X GET \
  https://api.site.devpanel.com/dev-v1/environments \
  -H "$AUTH_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' | jq 'map({ "_id": ._id, "name": .name })'
```

If you don't have access to any environments, you'll get the following output:
```
$ ./list_environments.sh
[]
```

But if you do have access to some environments, you'll get a more detailed output...

```
$ ./list_environments.sh
[
  {
    "_id": "5f68e1b2fb6a1a001c758",
    "name": "Free 4 Hour Testing Cluster"
  },
  {
    "_id": "5f8dc549e82f8d0015cce",
    "name": "Demo 1 Cluster"
  },
  {
    "_id": "603f38bcc0dd880014c21",
    "name": "Demo 2 Cluster"
  }
]
```

!!!! The API normally returns much more information, but we're limiting the information that's displayed using the `jq` command - the `jq` command is parsing out (\_id) and (name) fields and displaying just those.

Without using the `jq 'map({ "_id": ._id, "name": .name })'` command to limit the display fields, you can expect to see all the following details for each environment:

```
$ ./list_environments.sh
[
  {
    "_id": "5f68e1b2fb6a1a001c758",
    "isCluster": true,
    "isTesting": true,
    "tags": [],
    "minimumNodes": 3,
    "maximumNodes": 3,
    "desiredNodes": 3,
    "name": "Free 4 Hour Testing Cluster",
    "createdBy": "github|54149",
    "workspace": "5f68da685dd7810e250db",
    "applicationStack": "5cf673531c9d440000382",
    "status": "ACTIVE",
    "region": "us-west-2",
    "clusterName": "devpanel-cluster-vvybh",
    "instanceType": "t2.medium",
    "isEnableELKStack": true,
    "kibanaHostname": "kibana-devpanel-cluster-axldr.app.devpanel.com",
    "dbInstanceType": "db.t2.medium",
    "dbUserPassword": "password",
    "dbUsername": "dbadmin",
    "dbStorage": "20",
    "createdAt": "2020-09-21T17:24:02.978Z",
    "updatedAt": "2020-12-04T17:06:26.380Z",
    "lastActivity": {
      "_id": "5de106b3613fe71bf8787",
      "action": "CREATE_WORKSPACE",
      "createdBy": "github|9509",
      "workspace": "5de106b3613fe71bf8787",
      "status": "SUCCESS",
      "createdAt": "2019-11-29T11:53:23.684Z",
      "updatedAt": "2019-11-29T11:53:23.684Z"
    },
    "isProtected": true,
    "elb": "cc9ec8ea8-b481a1fd0d3963d7.elb.us-west-2.amazonaws.com",
    "arn": "arn:aws:iam::11111111:role/devpanelrole-99999-DevPanelRole-AAAAA",
    "tenant": "apidemo-1619593366-org"
  }
]
```


















