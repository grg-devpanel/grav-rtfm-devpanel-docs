---
title: 'List Branches API'
description: 'List of branches in a site'
taxonomy:
    category:
        - docs
---

# List Branches API
Here's how you can see the list of branches in a site with this API...

We will also look at the different types of data you can get on branches and the different levels of details.

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.


## Summary of Branches
```
$ cat list_branches_summary.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/609bfb67e265000013f0f/branches' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq 'map({ "_id": ._id, "name": .name, "status": .status })'
```

!!!! Here, `609bfb67e265000013f0f` is the Site ID

When run, this script will generate the following output: 

```
$ ./list_branches_summary.sh
[
  {
    "_id": "609bfb68e265000013f0f",
    "name": "master",
    "status": null
  }
]
```

!!!! Here, `"status": null` means that this branch is not built out yet.

BTW, remember that if this script was run without `'map({ "_id": ._id, "name": .name, "status": .status })'` parameters to the `jq` command, then we would see much more details in the output. (shown below)

```
$ ./list_branches_summary.sh
[
  {
    "_id": "609bfb68e265000013f0f",
    "isEnableEditor": true,
    "isEnablePMA": true,
    "lock": false,
    "doNotIndex": false,
    "secureConnection": false,
    "project": "609bfb67e265000013f0f",
    "name": "master",
    "branchType": "MASTER",
    "originBranch": "master",
    "createdAt": "2021-05-12T15:59:36.875Z",
    "updatedAt": "2021-05-12T15:59:36.875Z",
    "customDomains": []
  }
]
```

!!!! The above output is the output you would get after building out the master branch.

## Deployed Branches
```
$ cat list_branches_deployed.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/deployed-branches' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq 'map({ "_id": ._id, "name": .name, "status": .status })'
```

Running the above script would generate the following output: 

```
$ ./list_branches_deployed.sh
[
  {
    "_id": "6099303bc25947001370ca0d",
    "name": "master",
    "status": "DEPLOY_APPLICATION_SUCCESS"
  },
  {
    "_id": "60993084c25947001370ca15",
    "name": "feature2",
    "status": "DEPLOY_APPLICATION_SUCCESS"
  }
]
```

## Branch Details
Now here's all the detailed data you can get for each branch... If we were to run the summary script without `'map({ "_id": ._id, "name": .name, "status": .status })'` parameters to the `jq` command, then we would get the following output.

Here's the script - note that there are no parameters to the `jq` command...
```
$ cat list_branches.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

Here's the output for ONE branch: 
```
[
  {
    "_id": "6099303bc25947001370c",
    "isEnableEditor": true,
    "isEnablePMA": true,
    "lock": false,
    "doNotIndex": null,
    "secureConnection": false,
    "project": "60993039c25947001370c",
    "name": "master",
    "branchType": "MASTER",
    "originBranch": "master",
    "createdAt": "2021-05-10T13:08:11.211Z",
    "updatedAt": "2021-05-11T11:06:20.742Z",
    "appSettings": {
      "type": "drupal9",
      "adminUser": "devpanel",
      "adminPassword": "password"
    },
    "applicationName": "master-wscyl",
    "appserverImage": "wodby/php",
    "appserverTag": "7.4",
    "backupInterval": null,
    "copyDatabaseFilesApplication": null,
    "copyDatabaseFilesType": null,
    "copyDatabaseUrl": null,
    "copyFilesUrl": null,
    "customDomain": null,
    "dbName": "dbname",
    "dbPassword": "dbpass",
    "dbUser": "dbuser",
    "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "deploymentConfig": {
      "application": {
        "storage": "20Gi"
      },
      "images": {
        "nginx": {
          "env": [
            {
              "name": "NGINX_SERVER_ROOT",
              "value": "/var/www/html/web"
            },
            {
              "name": "NGINX_VHOST_PRESET",
              "value": "drupal9"
            },
            {
              "name": "NGINX_BACKEND_HOST",
              "value": "0.0.0.0"
            },
            {
              "name": "NGINX_SET_REAL_IP_FROM",
              "value": "10.0.0.0/16"
            }
          ]
        },
        "php": {
          "env": [
            {
              "name": "APP_ROOT",
              "value": "/var/www/html/"
            },
            {
              "name": "WEB_ROOT",
              "value": "/var/www/html/web"
            }
          ]
        }
      }
    },
    "editorHostname": "cs-master-wscylanm.app.devpanel.com",
    "editorPwd": "devpanel",
    "environment": "5f68e1b2fb6a1a001c758",
    "fromApplicationName": "undefined-wscylanm",
    "fromDB": null,
    "hostname": "dev-1370ca04-1370ca0a-1370c.app.devpanel.com",
    "isEnablePolymorphing": false,
    "isEnablePolyscripting": false,
    "lockPassword": null,
    "lockUsername": null,
    "namespace": "devpanel-1620651997078jykm",
    "oldHostname": null,
    "pmaHostname": "pma-master-wscyl.app.devpanel.com",
    "replicas": 1,
    "webserverImage": "wodby/nginx",
    "webserverTag": "1.17",
    "lastActivity": {
      "_id": "609a652919e933001431a",
      "action": "UPDATE_CONFIG_APPLICATION",
      "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
      "workspace": "60992fddc25947001370c",
      "project": "60993039c25947001370c",
      "application": "6099303bc25947001370c",
      "environment": "5f68e1b2fb6a1a001c758",
      "status": "SUCCESS",
      "createdAt": "2021-05-11T11:06:17.538Z",
      "updatedAt": "2021-05-11T11:06:49.223Z",
      "lastResponse": {
        "id": "609a652919e933001431a",
        "status": "SUCCESS",
        "payload": {},
        "task": "UPDATE_CONFIG_APPLICATION"
      },
      "taskName": null
    },
    "status": "DEPLOY_APPLICATION_SUCCESS",
    "customDomains": [
      {
        "_id": "609a63f242221c001328c",
        "domain": "api-site-v1.site.devpanel.com",
        "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
        "workspace": "60992fddc25947001370c",
        "application": "6099303bc25947001370c",
        "privateKey": "",
        "sslCertificate": "",
        "createdAt": "2021-05-11T11:01:06.970Z",
        "updatedAt": "2021-05-11T11:01:06.970Z"
      }
    ],
    "elb": "cfaf37f88f428f96cc84cc9ec8-b481a1fd0d396.elb.us-west-2.amazonaws.com"
  }
]
```

!!!! This output, as you can see, contains a lot of meta-data that might not be relavant to you, so we encourage you to pipe this output to a JSON parser like `jq` and extract only the information that you need.

