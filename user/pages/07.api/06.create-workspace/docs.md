---
title: 'Create workspace'
description: 'Create workspace'
taxonomy:
    category:
        - docs
---

# Create workspace
The following sample script shows how you can create a workspace using an API.

```
$ cat create_workspace.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

# it will create a demo workspace in Devpanel Demo Cluster and can see in UCB organization
curl -s -X POST \
  https://api.site.devpanel.com/dev-v1/workspaces \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  --data-raw '{
    "name":"API Demo 2",
    "description":"API Demo 2",
    "tags":[{"name":"demo"}],
    "isProtected":true,
    "accountType":"5f68da685dd7810e250db",
    "awsRoleArn":"5f68da685dd7810e250db",
    "region":"us-west-1",
    "minimumNodes":0,
    "maximumNodes":0,
    "dbInstanceType":"none",
    "setupType":"5f68e1b2fb6a1a001c758",
    "engine":"mysql",
    "engineVersion":"5.7",
    "parameterGroup":"default.mysql5.7",
    "agreeTermAndCondition":true,
    "teams":[],"collaborators":[]}' | jq
```

## Response

```
{
  "tags": [
    "5ec4069756b3d30014724"
  ],
  "isProtected": true,
  "isTesting": false,
  "status": "INACTIVE",
  "_id": "609bfa99e265000013f0f",
  "name": "API Demo 2",
  "description": "API Demo 2",
  "accountType": "5f68da685dd7810e250db",
  "teams": [],
  "owner": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "namespace": "devpanel-1620834968632eujsi",
  "tenant": "api-1620640441-org",
  "environment": "5f68e1b2fb6a1a001c758",
  "collaborators": [],
  "createdAt": "2021-05-12T15:56:09.229Z",
  "updatedAt": "2021-05-12T15:56:09.508Z",
  "ownerInfo": {
    "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "email_verified": "true",
    "email": "user@example.com",
    "role": "INSTANCE_ADMIN",
    "status": "ACTIVE",
    "user_metadata": {
      "githubAccessToken": "gho_nTQOIWRwwIuUGeIPKR"
    }
  }
}

```
