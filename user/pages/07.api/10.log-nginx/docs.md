---
title: 'Get Nginx Log of Branch'
description: 'How to get Nginx Log of a branch'
taxonomy:
    category:
        - docs
---

# Get Nginx Log of a Branch
The following sample script shows how you can get a branch's Nginx Log file...

```
$ cat get_branch_log_nginx.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/logs?toolName=nginx' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq ".data"
```


## Response

```
[
  "2021-05-11T04:30:15.239012160Z 205.169.39.17 - - [11/May/2021:04:30:15 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.71 Safari/537.36\"",
  "2021-05-11T06:24:02.345461736Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\"",
  "2021-05-11T06:24:02.387229271Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\"",
  "2021-05-11T06:24:02.608040280Z 217.170.206.112 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
  "2021-05-11T06:24:02.680090175Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\"",
  "2021-05-11T06:24:02.791210373Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
  "2021-05-11T06:24:02.795088895Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\"",
  "2021-05-11T06:24:02.795294249Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\"",
  "2021-05-11T06:24:02.807207983Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\"",
  "2021-05-11T06:24:02.811726128Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
  "2021-05-11T06:24:03.064114861Z 217.170.206.112 - - [11/May/2021:06:24:03 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
  "2021-05-11T09:39:06.005383013Z 115.79.28.176 - - [11/May/2021:09:39:06 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\""
]

```

## Without map
Here's the output that you would get without `jq ".data"`

```
{
  "podNamesList": [
    {
      "name": "master-wscyl-54c7979-pcntl",
      "resourceVersion": "78859",
      "metadata": {
        "annotations": {
          "kubernetes.io/psp": "eks.privileged"
        },
        "creationTimestamp": "2021-05-11T04:24:14.000Z",
        "generateName": "master-wscyl-54c7979-",
        "labels": {
          "app.kubernetes.io/instance": "master-wscyl",
          "app.kubernetes.io/name": "lemp",
          "pod-template-hash": "54c7979"
        },
        "managedFields": [
          {
            "apiVersion": "v1",
            "fieldsType": "FieldsV1",
            "fieldsV1": {
              "f:metadata": {
                "f:generateName": {},
                "f:labels": {
                  ".": {},
                  "f:app.kubernetes.io/instance": {},
                  "f:app.kubernetes.io/name": {},
                  "f:pod-template-hash": {}
                },
                "f:ownerReferences": {
                  ".": {},
                  "k:{\"uid\":\"6d3c000a-3018-4fc5-8267-0c4a0c7b2\"}": {
                    ".": {},
                    "f:apiVersion": {},
                    "f:blockOwnerDeletion": {},
                    "f:controller": {},
                    "f:kind": {},
                    "f:name": {},
                    "f:uid": {}
                  }
                }
              },
              "f:spec": {
                "f:containers": {
                  "k:{\"name\":\"nginx\"}": {
                    ".": {},
                    "f:env": {
                      ".": {},
                      "k:{\"name\":\"NGINX_BACKEND_HOST\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_SERVER_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_SET_REAL_IP_FROM\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"NGINX_VHOST_PRESET\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      }
                    },
                    "f:image": {},
                    "f:imagePullPolicy": {},
                    "f:name": {},
                    "f:ports": {
                      ".": {},
                      "k:{\"containerPort\":80,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:name": {},
                        "f:protocol": {}
                      }
                    },
                    "f:resources": {
                      ".": {},
                      "f:limits": {
                        ".": {},
                        "f:ephemeral-storage": {}
                      },
                      "f:requests": {
                        ".": {},
                        "f:cpu": {},
                        "f:ephemeral-storage": {},
                        "f:memory": {}
                      }
                    },
                    "f:terminationMessagePath": {},
                    "f:terminationMessagePolicy": {},
                    "f:volumeMounts": {
                      ".": {},
                      "k:{\"mountPath\":\"/var/www/html\"}": {
                        ".": {},
                        "f:mountPath": {},
                        "f:name": {}
                      }
                    }
                  },
                  "k:{\"name\":\"php\"}": {
                    ".": {},
                    "f:env": {
                      ".": {},
                      "k:{\"name\":\"APP_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"CODES_ENABLE\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"DB_HOST\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_NAME\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_PASSWORD\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DB_USER\"}": {
                        ".": {},
                        "f:name": {},
                        "f:valueFrom": {
                          ".": {},
                          "f:secretKeyRef": {
                            ".": {},
                            "f:key": {},
                            "f:name": {}
                          }
                        }
                      },
                      "k:{\"name\":\"DEVPANEL_HOSTNAME\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      },
                      "k:{\"name\":\"WEB_ROOT\"}": {
                        ".": {},
                        "f:name": {},
                        "f:value": {}
                      }
                    },
                    "f:image": {},
                    "f:imagePullPolicy": {},
                    "f:name": {},
                    "f:ports": {
                      ".": {},
                      "k:{\"containerPort\":8080,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:name": {},
                        "f:protocol": {}
                      },
                      "k:{\"containerPort\":9000,\"protocol\":\"TCP\"}": {
                        ".": {},
                        "f:containerPort": {},
                        "f:protocol": {}
                      }
                    },
                    "f:resources": {
                      ".": {},
                      "f:limits": {
                        ".": {},
                        "f:ephemeral-storage": {}
                      },
                      "f:requests": {
                        ".": {},
                        "f:cpu": {},
                        "f:ephemeral-storage": {},
                        "f:memory": {}
                      }
                    },
                    "f:terminationMessagePath": {},
                    "f:terminationMessagePolicy": {},
                    "f:volumeMounts": {
                      ".": {},
                      "k:{\"mountPath\":\"/var/www/html\"}": {
                        ".": {},
                        "f:mountPath": {},
                        "f:name": {}
                      }
                    }
                  }
                },
                "f:dnsPolicy": {},
                "f:enableServiceLinks": {},
                "f:nodeSelector": {
                  ".": {},
                  "f:groupType": {}
                },
                "f:restartPolicy": {},
                "f:schedulerName": {},
                "f:securityContext": {},
                "f:serviceAccount": {},
                "f:serviceAccountName": {},
                "f:terminationGracePeriodSeconds": {},
                "f:volumes": {
                  ".": {},
                  "k:{\"name\":\"app-volume\"}": {
                    ".": {},
                    "f:name": {},
                    "f:persistentVolumeClaim": {
                      ".": {},
                      "f:claimName": {}
                    }
                  }
                }
              }
            },
            "manager": "kube-controller-manager",
            "operation": "Update",
            "time": "2021-05-11T04:24:14.000Z"
          },
          {
            "apiVersion": "v1",
            "fieldsType": "FieldsV1",
            "fieldsV1": {
              "f:status": {
                "f:conditions": {
                  "k:{\"type\":\"ContainersReady\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  },
                  "k:{\"type\":\"Initialized\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  },
                  "k:{\"type\":\"Ready\"}": {
                    ".": {},
                    "f:lastProbeTime": {},
                    "f:lastTransitionTime": {},
                    "f:status": {},
                    "f:type": {}
                  }
                },
                "f:containerStatuses": {},
                "f:hostIP": {},
                "f:phase": {},
                "f:podIP": {},
                "f:podIPs": {
                  ".": {},
                  "k:{\"ip\":\"10.0.7.158\"}": {
                    ".": {},
                    "f:ip": {}
                  }
                },
                "f:startTime": {}
              }
            },
            "manager": "kubelet",
            "operation": "Update",
            "time": "2021-05-11T04:24:27.000Z"
          }
        ],
        "name": "master-wscyl-54c7979755-pc",
        "namespace": "devpanel-1620651997078jykm",
        "ownerReferences": [
          {
            "apiVersion": "apps/v1",
            "blockOwnerDeletion": true,
            "controller": true,
            "kind": "ReplicaSet",
            "name": "master-wscyl-54c7979",
            "uid": "6d3c000a-3018-4fc5-8267-0c4a0c7b2"
          }
        ],
        "resourceVersion": "78859",
        "selfLink": "/api/v1/namespaces/devpanel-1620651997078jykm/pods/master-wscyl-54c7979-pc",
        "uid": "434ed142-2108-426a-8d9b-8b63d4723"
      }
    }
  ],
  "data": [
    "2021-05-11T04:30:15.239012160Z 205.169.39.17 - - [11/May/2021:04:30:15 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.71 Safari/537.36\"",
    "2021-05-11T06:24:02.345461736Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\"",
    "2021-05-11T06:24:02.387229271Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\"",
    "2021-05-11T06:24:02.608040280Z 217.170.206.112 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
    "2021-05-11T06:24:02.680090175Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\"",
    "2021-05-11T06:24:02.791210373Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
    "2021-05-11T06:24:02.795088895Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\"",
    "2021-05-11T06:24:02.795294249Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\"",
    "2021-05-11T06:24:02.807207983Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\"",
    "2021-05-11T06:24:02.811726128Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
    "2021-05-11T06:24:03.064114861Z 217.170.206.112 - - [11/May/2021:06:24:03 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\"",
    "2021-05-11T09:39:06.005383013Z 115.79.28.176 - - [11/May/2021:09:39:06 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\""
  ],
  "events": [
    {
      "message": "2021-05-11T04:30:15.239012160Z 205.169.39.17 - - [11/May/2021:04:30:15 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.71 Safari/537.36\""
    },
    {
      "message": "2021-05-11T06:24:02.345461736Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\""
    },
    {
      "message": "2021-05-11T06:24:02.387229271Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\""
    },
    {
      "message": "2021-05-11T06:24:02.608040280Z 217.170.206.112 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\""
    },
    {
      "message": "2021-05-11T06:24:02.680090175Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\""
    },
    {
      "message": "2021-05-11T06:24:02.791210373Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\""
    },
    {
      "message": "2021-05-11T06:24:02.795088895Z 195.213.168.232 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1\""
    },
    {
      "message": "2021-05-11T06:24:02.795294249Z 193.36.118.251 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\""
    },
    {
      "message": "2021-05-11T06:24:02.807207983Z 185.104.184.125 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\""
    },
    {
      "message": "2021-05-11T06:24:02.811726128Z 104.129.56.139 - - [11/May/2021:06:24:02 +0000] \"GET / HTTP/1.1\" 200 8721 \"http://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\""
    },
    {
      "message": "2021-05-11T06:24:03.064114861Z 217.170.206.112 - - [11/May/2021:06:24:03 +0000] \"GET / HTTP/1.1\" 200 8853 \"https://dev-1370ca04-1370ca0a-1370c.app.devpanel.com/\" \"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1\""
    },
    {
      "message": "2021-05-11T09:39:06.005383013Z 115.79.28.176 - - [11/May/2021:09:39:06 +0000] \"GET / HTTP/1.1\" 200 2769 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\""
    }
  ],
  "sessionId": "609c09b5e265000013f0f"
}

```
