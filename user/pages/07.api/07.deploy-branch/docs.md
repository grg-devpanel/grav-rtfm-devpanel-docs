---
title: 'Deploy Branch'
description: 'Deploy Branch'
taxonomy:
    category:
        - docs
---

# Deploy Branch

The following sample script shows how you can deploy a branch using an API.

We will look at some different ways of branches as well.

## Deploy branch from scratch

```
$ cat deploy_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099307dc25947001370c' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
  "lock": false,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "isEnableEditor": true,
  "isEnablePMA": true,
  "isEnableStableHostname": true,
  "deploymentConfig": {
    "application": { "storage": "20Gi" },
    "images": {
      "nginx": {
        "env": [
          { "name": "NGINX_SERVER_ROOT", "value": "/var/www/html/web" },
          { "name": "NGINX_VHOST_PRESET", "value": "drupal9" },
          { "name": "NGINX_BACKEND_HOST", "value": "0.0.0.0" },
          { "name": "NGINX_SET_REAL_IP_FROM", "value": "10.0.0.0/16" }
        ]
      },
      "php": {
        "env": [
          { "name": "APP_ROOT", "value": "/var/www/html/" },
          { "name": "WEB_ROOT", "value": "/var/www/html/web" }
        ]
      }
    }
  }
}' | jq
```

### Response
```
{
  "isEnableEditor": true,
  "isEnablePMA": true,
  "lock": false,
  "doNotIndex": null,
  "secureConnection": false,
  "_id": "6099307dc25947001370c",
  "name": "feature1",
  "branchType": "OTHER",
  "originBranch": "feature1",
  "project": "60993039c25947001370c",
  "createdAt": "2021-05-10T13:09:17.263Z",
  "updatedAt": "2021-05-12T16:05:13.591Z",
  "appSettings": {
    "type": "drupal9",
    "adminUser": "devpanel",
    "adminPassword": "pwd2ngosluxkngos"
  },
  "applicationName": "feature1-wscyl",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "backupInterval": null,
  "copyDatabaseFilesApplication": null,
  "copyDatabaseFilesType": null,
  "copyDatabaseUrl": null,
  "copyFilesUrl": null,
  "customDomain": null,
  "dbName": "namengos",
  "dbPassword": "pwdngos",
  "dbUser": "userngos",
  "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "deploymentConfig": {
    "application": {
      "storage": "20Gi"
    },
    "images": {
      "nginx": {
        "env": [
          {
            "name": "NGINX_SERVER_ROOT",
            "value": "/var/www/html/web"
          },
          {
            "name": "NGINX_VHOST_PRESET",
            "value": "drupal9"
          },
          {
            "name": "NGINX_BACKEND_HOST",
            "value": "0.0.0.0"
          },
          {
            "name": "NGINX_SET_REAL_IP_FROM",
            "value": "10.0.0.0/16"
          }
        ]
      },
      "php": {
        "env": [
          {
            "name": "APP_ROOT",
            "value": "/var/www/html/"
          },
          {
            "name": "WEB_ROOT",
            "value": "/var/www/html/web"
          }
        ]
      }
    }
  },
  "editorHostname": "cs-feature1-wscyl.app.devpanel.com",
  "editorPwd": "devpanel",
  "environment": "5f68e1b2fb6a1a001c758",
  "fromApplicationName": "undefined-wscy",
  "fromDB": null,
  "hostname": "dev-1370ca04-1370ca0a-1370c.app.devpanel.com",
  "isEnablePolymorphing": false,
  "isEnablePolyscripting": false,
  "lockPassword": null,
  "lockUsername": null,
  "namespace": "devpanel-1620651997078jykm",
  "oldHostname": null,
  "pmaHostname": "pma-feature1-wscy.app.devpanel.com",
  "replicas": 1,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "lastActivity": {
    "_id": "609bfcb759ae0000181be",
    "action": "DEPLOY_APPLICATION",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "60993039c25947001370c",
    "application": "6099307dc25947001370c",
    "environment": "5f68e1b2fb6a1a001c758",
    "status": "PENDING",
    "createdAt": "2021-05-12T16:05:11.925Z",
    "updatedAt": "2021-05-12T16:05:11.925Z"
  },
  "status": "UNDEPLOY_APPLICATION_SUCCESS"
}

```


## Deploy branch from an existing running branch

```
$ cat clone_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/60993070fff9de0013e8d' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
    "lock":false,
    "copyDatabaseFilesType":"APPLICATION",
    "copyDatabaseFilesApplication":"6099303bc25947001370c",
    "webserverImage":"wodby/nginx",
    "webserverTag":"1.17",
    "appserverImage":"wodby/php",
    "appserverTag":"7.4",
    "isEnableEditor":true,
    "isEnablePMA":true,
    "isEnableStableHostname":true,
    "deploymentConfig":{"application":{"storage":"20Gi"},"images":{"nginx":{"env":[{"name":"NGINX_SERVER_ROOT","value":"/var/www/html/web"},{"name":"NGINX_VHOST_PRESET","value":"drupal9"},{"name":"NGINX_BACKEND_HOST","value":"0.0.0.0"},{"name":"NGINX_SET_REAL_IP_FROM","value":"10.0.0.0/16"}]},"php":{"env":[{"name":"APP_ROOT","value":"/var/www/html/"},{"name":"WEB_ROOT","value":"/var/www/html/web"}]}}}
}' | jq
```

### Response

```
{
  "isEnableEditor": true,
  "isEnablePMA": true,
  "lock": false,
  "doNotIndex": null,
  "secureConnection": false,
  "_id": "60993070fff9de0013e8d",
  "name": "release",
  "branchType": "OTHER",
  "originBranch": "release",
  "project": "60993039c25947001370c",
  "createdAt": "2021-05-10T13:09:04.443Z",
  "updatedAt": "2021-05-13T10:50:26.358Z",
  "appSettings": {
    "type": "drupal9",
    "adminUser": "devpanel",
    "adminPassword": "pwd2oqnjnxko"
  },
  "applicationName": "release-wscy",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "backupInterval": null,
  "copyDatabaseFilesApplication": "6099303bc25947001370c",
  "copyDatabaseFilesType": "APPLICATION",
  "copyDatabaseUrl": null,
  "copyFilesUrl": null,
  "customDomain": null,
  "dbName": "nameajplsw",
  "dbPassword": "pwdajplsw",
  "dbUser": "userajplsw",
  "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "deploymentConfig": {
    "application": {
      "storage": "20Gi"
    },
    "images": {
      "nginx": {
        "env": [
          {
            "name": "NGINX_SERVER_ROOT",
            "value": "/var/www/html/web"
          },
          {
            "name": "NGINX_VHOST_PRESET",
            "value": "drupal9"
          },
          {
            "name": "NGINX_BACKEND_HOST",
            "value": "0.0.0.0"
          },
          {
            "name": "NGINX_SET_REAL_IP_FROM",
            "value": "10.0.0.0/16"
          }
        ]
      },
      "php": {
        "env": [
          {
            "name": "APP_ROOT",
            "value": "/var/www/html/"
          },
          {
            "name": "WEB_ROOT",
            "value": "/var/www/html/web"
          }
        ]
      }
    }
  },
  "editorHostname": "cs-release-wscyl.app.devpanel.com",
  "editorPwd": "devpanel",
  "environment": "5f68e1b2fb6a1a001c758",
  "fromApplicationName": "master-wscyl",
  "fromDB": "nameoqnj",
  "hostname": "dev-1370ca04-1370ca0a-13e8d.app.devpanel.com",
  "isEnablePolymorphing": false,
  "isEnablePolyscripting": false,
  "lockPassword": null,
  "lockUsername": null,
  "namespace": "devpanel-1620651997078jykm",
  "oldHostname": null,
  "pmaHostname": "pma-release-wscyl.app.devpanel.com",
  "replicas": 1,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "lastActivity": {
    "_id": "609d0470e265000013f0f",
    "action": "DEPLOY_APPLICATION",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "60993039c25947001370c",
    "application": "60993070fff9de0013e8d",
    "environment": "5f68e1b2fb6a1a001c758",
    "status": "PENDING",
    "createdAt": "2021-05-13T10:50:24.176Z",
    "updatedAt": "2021-05-13T10:50:24.176Z"
  },
  "status": "UNDEPLOY_APPLICATION_SUCCESS"
}

```

## Deploy branch from an existing s3 urls

```
$ cat deploy_branch_with_backups.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

# http url https://devpanel-cluster-vvybh.s3.amazonaws.com/master-wscyl/dump/thkbnq7yjdc9h.tgz
# database s3://devpanel-cluster-vvybh/master-wscyl/dump/thkbnq7yjdc9h.tgz

# http url https://devpanel-cluster-vvybh.s3.amazonaws.com/master-wscyl/dump/h7zytg4u9ewpa.tgz
# static file s3://devpanel-cluster-vvybh/master-wscyl/dump/h7zytg4u9ewpa.tgz

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/60993076fff9de0013e8d' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441141-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
    "lock":false,
    "copyDatabaseFilesType":"HTTP",
    "oldHostname":"dev-147b9a4f-147b9a55-147b9.app.devpanel.com",
    "copyDatabaseUrl":"s3://devpanel-cluster-vvybh/master-wscyl/dump/thkbnq7yjdc9h.tgz",
    "copyFilesUrl":"s3://devpanel-cluster-vvybh/master-wscyl/dump/h7zytg4u9ewpa.tgz",
    "webserverImage":"wodby/nginx",
    "webserverTag":"1.17",
    "appserverImage":"wodby/php",
    "appserverTag":"7.4",
    "isEnableEditor":true,
    "isEnablePMA":true,
    "isEnableStableHostname":true,
    "deploymentConfig":{"application":{"storage":"20Gi"},"images":{"nginx":{"env":[{"name":"NGINX_SERVER_ROOT","value":"/var/www/html/web"},{"name":"NGINX_VHOST_PRESET","value":"drupal9"},{"name":"NGINX_BACKEND_HOST","value":"0.0.0.0"},{"name":"NGINX_SET_REAL_IP_FROM","value":"10.0.0.0/16"}]},"php":{"env":[{"name":"APP_ROOT","value":"/var/www/html/"},{"name":"WEB_ROOT","value":"/var/www/html/web"}]}}}
}' | jq

```

### Response
```
{
  "isEnableEditor": true,
  "isEnablePMA": true,
  "lock": false,
  "doNotIndex": null,
  "secureConnection": false,
  "_id": "60993076fff9de0013e8d",
  "name": "develop",
  "branchType": "OTHER",
  "originBranch": "develop",
  "project": "60993039c25947001370c",
  "createdAt": "2021-05-10T13:09:10.302Z",
  "updatedAt": "2021-05-12T16:21:57.507Z",
  "appSettings": {
    "type": "drupal9",
    "adminUser": "devpanel",
    "adminPassword": "pwd2wuszzl"
  },
  "applicationName": "develop-wscyl",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "backupInterval": null,
  "copyDatabaseFilesApplication": null,
  "copyDatabaseFilesType": "HTTP",
  "copyDatabaseUrl": "s3://devpanel-cluster-vvybh/master-wscyl/dump/thkbnq7yjdc9h.tgz",
  "copyFilesUrl": "s3://devpanel-cluster-vvybh/master-wscyl/dump/h7zytg4u9ewpa.tgz",
  "customDomain": null,
  "dbName": "namewus",
  "dbPassword": "pwdwus",
  "dbUser": "userwus",
  "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "deploymentConfig": {
    "application": {
      "storage": "20Gi"
    },
    "images": {
      "nginx": {
        "env": [
          {
            "name": "NGINX_SERVER_ROOT",
            "value": "/var/www/html/web"
          },
          {
            "name": "NGINX_VHOST_PRESET",
            "value": "drupal9"
          },
          {
            "name": "NGINX_BACKEND_HOST",
            "value": "0.0.0.0"
          },
          {
            "name": "NGINX_SET_REAL_IP_FROM",
            "value": "10.0.0.0/16"
          }
        ]
      },
      "php": {
        "env": [
          {
            "name": "APP_ROOT",
            "value": "/var/www/html/"
          },
          {
            "name": "WEB_ROOT",
            "value": "/var/www/html/web"
          }
        ]
      }
    }
  },
  "editorHostname": "cs-develop-wscyl.app.devpanel.com",
  "editorPwd": "devpanel",
  "environment": "5f68e1b2fb6a1a001c758",
  "fromApplicationName": "undefined-wscyl",
  "fromDB": null,
  "hostname": "dev-1370ca04-1370ca0a-13e8d.app.devpanel.com",
  "isEnablePolymorphing": false,
  "isEnablePolyscripting": false,
  "lockPassword": null,
  "lockUsername": null,
  "namespace": "devpanel-1620651997078jykm",
  "oldHostname": "dev-147b9a4f-147b9a55-147b9.app.devpanel.com",
  "pmaHostname": "pma-develop-wscyl.app.devpanel.com",
  "replicas": 1,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "lastActivity": {
    "_id": "609c00a359ae0000181be",
    "action": "DEPLOY_APPLICATION",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "60993039c25947001370c",
    "application": "60993076fff9de0013e8d",
    "environment": "5f68e1b2fb6a1a001c758",
    "status": "PENDING",
    "createdAt": "2021-05-12T16:21:55.783Z",
    "updatedAt": "2021-05-12T16:21:55.783Z"
  },
  "status": "UNDEPLOY_APPLICATION_SUCCESS"
}

```
