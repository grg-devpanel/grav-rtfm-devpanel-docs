---
title: 'Get Site Costs'
description: 'Download the Cost per Site CSV file for your org'
taxonomy:
    category:
        - docs
---

# Get Site Costs 

The following sample script generates a CSV file that can be used to calculate per site cost for all sites in your org for a given month. 

```
$ cat get_cost_per_site_for_org.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441141-org"
curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/organizations/cost?month=2021-05' \
  -H 'cache-control: no-cache' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER"
```

In the above script, `api-1620640441141-org` is the "ORG ID" (or tenant) and `month=2021-05` is the month for which this data is being reported...

## Response

The response will be a CSV file that you can capture, save and edit with any spreadsheet.

```
"YYYYMM","Workspace Name","Project Name","Branch Name","Tags","Site ID","Branch ID","CDN Requests","Origin Hits","Total Hits","Costs","URL"
"202105","API Demo","Demo Drupal 9_1","master","demo","60993039c25947001370ca0a","6099303bc25947001370ca0d",0,16,16,"","dev-1370ca04-1370ca0a-1370ca0d.app.devpanel.com"
"202105","API Demo","Demo Drupal 9_1","develop","demo","60993039c25947001370ca0a","60993076fff9de0013e8df7d",0,0,0,"","site-develop-wscylanm.app.devpanel.com"
"202105","API Demo","Demo Drupal 9_1","feature2","demo","60993039c25947001370ca0a","60993084c25947001370ca15",0,16,16,"","dev-1370ca04-1370ca0a-1370ca15.app.devpanel.com"
"202105","API Demo","Demo Drupal 9","master","demo","609bfb67e265000013f0fd8e","609bfb68e265000013f0fd91",0,13,13,"","dev-1370ca04-13f0fd8e-13f0fd91.app.devpanel.com"
"202105","API Demo","Demo Drupal 9","release","demo","609bfb67e265000013f0fd8e","60a384f1bfa6050013af35fe",0,5,5,"","dev-1370ca04-13f0fd8e-13af35fe.app.devpanel.com"
"202105","API Demo","Demo Drupal 9","develop","demo","609bfb67e265000013f0fd8e","60a384f6bfa6050013af3603",102,51,153,"","dev-1370ca04-13f0fd8e-13af3603.app.devpanel.com"
"YYYYMM","Workspace Name","Project Name","Branch Name","Tags","Site ID","Branch ID",102,101,362,"","URL"
```

!!!! Copy/Paste the above CSV file (code block) into a spreadsheet to see how it works...

Site costs cloumn can be calculated with this formula in your spreadsheet:
Cost for Site X = (Hits for Site X / Total Hits) * Fees for Total Hits

If you are running all your sites in one AWS account dedicated to DevPanel, then "Fees for Total Hits" will be your account total.

This calculation assumes equal allocation of AWS resources for each site per hit.

Here is a sample spreadsheet demonstrating how to calculate your per site costs:
[devpanel_cost_per_site_api_-_calculated.xlsx](/misc/devpanel_cost_per_site_api_-_calculated.xlsx)
`From API` tab shows you what you'll get from the API
`Cost Calculation` tab shows you the formulas you'll have to use

![2021-05-19_15-27-29.png](2021-05-19_15-27-29.png)
