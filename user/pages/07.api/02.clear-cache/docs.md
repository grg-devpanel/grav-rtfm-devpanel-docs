---
title: 'Clear Cache'
description: 'Clear CDN cache for a branch'
taxonomy:
    category:
        - docs
---

# Clear Cache

! This API is still under development but might be available for preview. Please contact us for details.

The following sample script shows how you can clear a branch's CDN cache.

```
$ cat clear_cache_service.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441141-org"
curl -s -X PUT \
  'https://api.site.devpanel.com/dev-v1/sites/609bfb67e265000013f0fd8e/branches/60a384f6bfa6050013af3603/caches' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

In the above script, `60a384f6bfa6050013af3603` is the "Branch ID."

## Response
```
{
  "Location": "https://cloudfront.amazonaws.com/2020-05-31/distribution/E23BZO7WNF1GWL/invalidation/I918FIR2X8IW4",
  "Invalidation": {
    "Id": "I918FIR2X8IW4",
    "Status": "InProgress",
    "CreateTime": "2021-05-20T14:44:00.480Z",
    "InvalidationBatch": {
      "Paths": {
        "Quantity": 1,
        "Items": [
          "/*"
        ]
      },
      "CallerReference": "1621521837675"
    }
  }
}
```

