---
title: 'List Domains of Workspace'
description: 'List Domains of Workspace'
taxonomy:
    category:
        - docs
---

# List Domains in a Workspace
Here's how you can see the list of domains in a workspace...

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.

```
$ cat list_domains_workspace.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"

curl -s -X GET \
  https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370c/domains \
  -H "$AUTH_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H 'tenant: api-1620640441-org' | jq 'map({ "_id": ._id, "domain": .domain, "createdAt": .createdAt })'
  
 ```
 
 ## Response
 ```
 [
  {
    "_id": "609a63f242221c001328c",
    "domain": "api-site-v1.site.devpanel.com",
    "createdAt": "2021-05-11T11:01:06.970Z"
  }
]
```

## Without map
```
[
  {
    "_id": "609a63f242221c001328c",
    "domain": "api-site-v1.site.devpanel.com",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "application": {
      "_id": "6099303bc25947001370c",
      "isEnableEditor": true,
      "isEnablePMA": true,
      "lock": false,
      "doNotIndex": null,
      "secureConnection": false,
      "project": {
        "_id": "60993039c25947001370c",
        "tags": [
          "6093bed752409d00130c0"
        ],
        "name": "Demo Drupal 9_1",
        "description": "Demo Drupal 9",
        "sourceCodeToken": null,
        "repositoryName": "may10th_drupal_9",
        "repositoryProvider": "GITHUB",
        "projectType": "5f5600c4a19ceb6ef3d48",
        "repositoryOwner": "johnnydevpanel",
        "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
        "workspace": "60992fddc25947001370c",
        "friendlyName": "wscylanm",
        "repositoryId": "366046",
        "initialized": true,
        "createdAt": "2021-05-10T13:08:09.951Z",
        "updatedAt": "2021-05-12T16:21:57.587Z",
        "lastActivity": "609c00a359ae0000181b",
        "repositoryWebHookId": "296599"
      },
      "name": "master",
      "branchType": "MASTER",
      "originBranch": "master",
      "createdAt": "2021-05-10T13:08:11.211Z",
      "updatedAt": "2021-05-12T16:52:52.876Z",
      "appSettings": {
        "type": "drupal9",
        "adminUser": "devpanel",
        "adminPassword": "password"
      },
      "applicationName": "master-wscyl",
      "appserverImage": "wodby/php",
      "appserverTag": "7.4",
      "backupInterval": null,
      "copyDatabaseFilesApplication": null,
      "copyDatabaseFilesType": null,
      "copyDatabaseUrl": null,
      "copyFilesUrl": null,
      "customDomain": null,
      "dbName": "nameoqn",
      "dbPassword": "pwdoqn",
      "dbUser": "useroqn",
      "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
      "deploymentConfig": {
        "application": {
          "storage": "20Gi"
        },
        "images": {
          "nginx": {
            "env": [
              {
                "name": "NGINX_SERVER_ROOT",
                "value": "/var/www/html/web"
              },
              {
                "name": "NGINX_VHOST_PRESET",
                "value": "drupal9"
              },
              {
                "name": "NGINX_BACKEND_HOST",
                "value": "0.0.0.0"
              },
              {
                "name": "NGINX_SET_REAL_IP_FROM",
                "value": "10.0.0.0/16"
              }
            ]
          },
          "php": {
            "env": [
              {
                "name": "APP_ROOT",
                "value": "/var/www/html/"
              },
              {
                "name": "WEB_ROOT",
                "value": "/var/www/html/web"
              }
            ]
          }
        }
      },
      "editorHostname": "cs-master-wscyl.app.devpanel.com",
      "editorPwd": "devpanel",
      "environment": "5f68e1b2fb6a1a001c758",
      "fromApplicationName": "undefined-wscyl",
      "fromDB": null,
      "hostname": "dev-1370ca04-1370ca0a-1370c.app.devpanel.com",
      "isEnablePolymorphing": false,
      "isEnablePolyscripting": false,
      "lockPassword": null,
      "lockUsername": null,
      "namespace": "devpanel-1620651997078jykm",
      "oldHostname": null,
      "pmaHostname": "pma-master-wscyl.app.devpanel.com",
      "replicas": 1,
      "webserverImage": "wodby/nginx",
      "webserverTag": "1.17",
      "lastActivity": "609c07e3e265000013f0f",
      "status": "DEPLOY_APPLICATION_SUCCESS"
    },
    "privateKey": "",
    "sslCertificate": "",
    "createdAt": "2021-05-11T11:01:06.970Z",
    "updatedAt": "2021-05-11T11:01:06.970Z",
    "createdByInfo": {
      "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
      "email_verified": "true",
      "email": "user@example.com",
      "role": "INSTANCE_ADMIN",
      "status": "ACTIVE",
      "user_metadata": {
        "githubAccessToken": "gho_nTQOIWRwwIuUGeIP"
      }
    },
    "environment": {
      "_id": "5f68e1b2fb6a1a001c758",
      "isCluster": true,
      "isTesting": true,
      "tags": [],
      "minimumNodes": 3,
      "maximumNodes": 3,
      "desiredNodes": 3,
      "name": "Free 4 Hour Testing Cluster",
      "createdBy": "github|54149",
      "workspace": "5f68da685dd7810e250db",
      "applicationStack": "5cf673531c9d440000382",
      "status": "ACTIVE",
      "region": "us-west-2",
      "clusterName": "devpanel-cluster-vvybh",
      "instanceType": "t2.medium",
      "isEnableELKStack": true,
      "kibanaHostname": "kibana-devpanel-cluster-axldr.app.devpanel.com",
      "dbInstanceType": "db.t2.medium",
      "dbUserPassword": "devpanelwbh",
      "dbUsername": "admin",
      "dbStorage": "20",
      "createdAt": "2020-09-21T17:24:02.978Z",
      "updatedAt": "2020-12-04T17:06:26.380Z",
      "lastActivity": "5de106b3613fe71bf8787",
      "isProtected": true,
      "elb": "afccfaf37f88f428f96cc84cc9ec8-b481a1fd0d396.elb.us-west-2.amazonaws.com",
      "arn": "arn:aws:iam::295857545:role/devpanelrole-1592204684-DevPanelRole-55FSWFP1I",
      "tenant": "demo-1619593366-org"
    }
  }
]
```
