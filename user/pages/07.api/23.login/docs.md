---
title: 'Login API'
description: 'How to Login using the API'
taxonomy:
    category:
        - docs
---

# Login API
Here's how you can login using the API and get your JWT Auth Token...

> The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.
{.is-warning}


```
$ cat login.sh
#!/bin/bash

curl -sX POST \
  https://api.site.devpanel.com/dev-v1/login \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"username": "user@example.com",
	"password": "password" }' | jq '.AccessToken' --raw-output > JWT_TOKEN.txt
```

In this example, we've created a script called `login.sh` that, when run, will create a file called `JWT_TOKEN.txt` with the user's Auth Token. 

!!!! Note that the username and password are hard-coded in the demo script above... You wouldn't want to do that in production scripts.

The `JWT_TOKEN.txt` like this (below) and is used in all the following demo scripts.

```
$ ./login.sh
$ cat JWT_TOKEN.txt
eyJraWQiOiIrVmRQUmxmemFhdjU2aW12WTlZQWdTNHhpVEdlXC9UeHdZaEZNQUxyWXc5WT0iLCJhbGciOi...
```


