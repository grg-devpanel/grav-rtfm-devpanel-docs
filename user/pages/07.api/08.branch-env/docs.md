---
title: 'Get Branch Environment Variables'
description: 'Get Branch Environment Variables'
taxonomy:
    category:
        - docs
---

# Get Branch Environment Variables

The following sample script shows how you can get a branch's Environment Variables.

```
$ cat get_meta_branch_variables.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/variables' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

## Response

```
[
  {
    "name": "DEVPANEL_HOSTNAME",
    "value": "dev-1370ca04-1370ca0a-1370c.app.devpanel.com"
  },
  {
    "name": "APP_ROOT",
    "value": "/var/www/html/"
  },
  {
    "name": "WEB_ROOT",
    "value": "/var/www/html/web"
  },
  {
    "name": "CODES_ENABLE",
    "value": "yes"
  },
  {
    "name": "DB_HOST",
    "valueFrom": {
      "secretKeyRef": {
        "key": "DB_HOST",
        "name": "master-wscyl"
      }
    }
  },
  {
    "name": "DB_NAME",
    "valueFrom": {
      "secretKeyRef": {
        "key": "DB_NAME",
        "name": "master-wscyl"
      }
    }
  },
  {
    "name": "DB_USER",
    "valueFrom": {
      "secretKeyRef": {
        "key": "DB_USER",
        "name": "master-wscyl"
      }
    }
  },
  {
    "name": "DB_PASSWORD",
    "valueFrom": {
      "secretKeyRef": {
        "key": "DB_PASSWORD",
        "name": "master-wscyl"
      }
    }
  }
]
```

Note that the values for `DB_HOST`, `DB_NAME`, `DB_USER`, and `DB_PASSWORD` variables are stored in the `master-wscyl` secret store. To get these values, you can use the `command` API and run `echo` commands in the branch's container. 
