---
title: 'Create backup files of Branch'
description: 'Create backup files of Branch'
taxonomy:
    category:
        - docs
---

# Create backup files of Branch
The following sample script shows how you can create backups of branch database and files using an API.


## Create backup database files

```
$ cat backup_db_from_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/databases' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

### Response 

```
{
  "status": "ok"
}

```

## Create backup static file

```
$ cat backup_file_from_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/files' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

### Response 

```
{
  "status": "ok"
}
```

