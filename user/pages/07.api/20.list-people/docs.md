---
title: 'List People of a Workspace'
description: 'List People of a Workspace'
taxonomy:
    category:
        - docs
---

# List People in a Workspace
Here's how you can see the list of people who have access to a workspace...

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.

```
$ cat list_members_in_wp.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"

curl -s -X GET \
  https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370c/people \
  -H "$AUTH_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H 'tenant: api-1620640441-org' | jq 'map({ "role": .role, "collaborator.email": .collaborator.email })'
  ```
  
## Response
```
[
  {
    "role": "ADMIN",
    "collaborator.email": "user@example.com"
  }
]
```

## Without map
```
[
  {
    "collaborator": {
      "sub": "95a3b391-5bdb-4f46-8931-2ab",
      "email_verified": "true",
      "email": "user@example.com",
      "role": "INSTANCE_ADMIN",
      "status": "ACTIVE",
      "user_metadata": {
        "githubAccessToken": "gho_nTQOIWRww"
      }
    },
    "role": "ADMIN",
    "isOwner": true,
    "isCreator": true,
    "workspace": "60992fddc25947001370c"
  }
]
```
