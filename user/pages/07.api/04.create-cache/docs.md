---
title: 'Create Cache Service for Branch'
description: 'Create Cache Service for Branch'
taxonomy:
    category:
        - docs
---

# Create Cache Service for Branch

The following sample script shows how you can create a Cache Service (CDN) for an application using an API.

```
$ cat create_cache_service.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/609bfb67e265000013f0fd8e/branches/60a384f6bfa6050013af3603/caches' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
  
```

## Response 

```
{
  "cdn": "d1i18k9umfr09s.cloudfront.net",
  "config": {
    "_id": "60a673435f22da10998ba6d6",
    "name": "ELB-afccfaf37f88f428f96cc84cc9ec8ea8-b481a1fd0d3963d7.elb.us-west-2.amazonaws.com",
    "type": "cloudfront",
    "config": {
      "Location": "https://cloudfront.amazonaws.com/2020-05-31/distribution/E23BZO7WNF1GWL",
      "ETag": "EHVPKB349AHOK",
      "Distribution": {
        "Id": "E23BZO7WNF1GWL",
        "ARN": "arn:aws:cloudfront::295857545527:distribution/E23BZO7WNF1GWL",
        "Status": "InProgress",
        "LastModifiedTime": "2021-05-20T14:33:38.736Z",
        "InProgressInvalidationBatches": 0,
        "DomainName": "d1i18k9umfr09s.cloudfront.net",
        "ActiveTrustedSigners": {
          "Enabled": false,
          "Quantity": 0,
          "Items": []
        },
        "ActiveTrustedKeyGroups": {
          "Enabled": false,
          "Quantity": 0,
          "Items": []
        },
        "DistributionConfig": {
          "CallerReference": "60a384f6bfa6050013af36031621521216233",
          "Aliases": {
            "Quantity": 0,
            "Items": []
          },
          "DefaultRootObject": "",
          "Origins": {
            "Quantity": 1,
            "Items": [
              {
                "Id": "ELB-afccfaf37f88f428f96cc84cc9ec8ea8-b481a1fd0d3963d7.elb.us-west-2.amazonaws.com",
                "DomainName": "afccfaf37f88f428f96cc84cc9ec8ea8-b481a1fd0d3963d7.elb.us-west-2.amazonaws.com",
                "OriginPath": "",
                "CustomHeaders": {
                  "Quantity": 0,
                  "Items": []
                },
                "CustomOriginConfig": {
                  "HTTPPort": 80,
                  "HTTPSPort": 443,
                  "OriginProtocolPolicy": "match-viewer",
                  "OriginSslProtocols": {
                    "Quantity": 3,
                    "Items": [
                      "TLSv1",
                      "TLSv1.1",
                      "TLSv1.2"
                    ]
                  },
                  "OriginReadTimeout": 30,
                  "OriginKeepaliveTimeout": 5
                },
                "ConnectionAttempts": 3,
                "ConnectionTimeout": 10,
                "OriginShield": {
                  "Enabled": false
                }
              }
            ]
          },
          "OriginGroups": {
            "Quantity": 0,
            "Items": []
          },
          "DefaultCacheBehavior": {
            "TargetOriginId": "ELB-afccfaf37f88f428f96cc84cc9ec8ea8-b481a1fd0d3963d7.elb.us-west-2.amazonaws.com",
            "TrustedSigners": {
              "Enabled": false,
              "Quantity": 0,
              "Items": []
            },
            "TrustedKeyGroups": {
              "Enabled": false,
              "Quantity": 0,
              "Items": []
            },
            "ViewerProtocolPolicy": "allow-all",
            "AllowedMethods": {
              "Quantity": 7,
              "Items": [
                "HEAD",
                "DELETE",
                "POST",
                "GET",
                "OPTIONS",
                "PUT",
                "PATCH"
              ],
              "CachedMethods": {
                "Quantity": 3,
                "Items": [
                  "HEAD",
                  "GET",
                  "OPTIONS"
                ]
              }
            },
            "SmoothStreaming": false,
            "Compress": true,
            "LambdaFunctionAssociations": {
              "Quantity": 0,
              "Items": []
            },
            "FunctionAssociations": {
              "Quantity": 0,
              "Items": []
            },
            "FieldLevelEncryptionId": "",
            "ForwardedValues": {
              "QueryString": false,
              "Cookies": {
                "Forward": "all"
              },
              "Headers": {
                "Quantity": 4,
                "Items": [
                  "Origin",
                  "CloudFront-Forwarded-Proto",
                  "Host",
                  "Accept-Encoding"
                ]
              },
              "QueryStringCacheKeys": {
                "Quantity": 0,
                "Items": []
              }
            },
            "MinTTL": 0,
            "DefaultTTL": 86400,
            "MaxTTL": 31536000
          },
          "CacheBehaviors": {
            "Quantity": 0,
            "Items": []
          },
          "CustomErrorResponses": {
            "Quantity": 0,
            "Items": []
          },
          "Comment": "",
          "Logging": {
            "Enabled": false,
            "IncludeCookies": false,
            "Bucket": "",
            "Prefix": ""
          },
          "PriceClass": "PriceClass_All",
          "Enabled": true,
          "ViewerCertificate": {
            "CloudFrontDefaultCertificate": true,
            "MinimumProtocolVersion": "TLSv1",
            "CertificateSource": "cloudfront"
          },
          "Restrictions": {
            "GeoRestriction": {
              "RestrictionType": "none",
              "Quantity": 0,
              "Items": []
            }
          },
          "WebACLId": "",
          "HttpVersion": "http2",
          "IsIPV6Enabled": true
        },
        "AliasICPRecordals": []
      }
    },
    "createdAt": "2021-05-20T14:33:39.258Z",
    "updatedAt": "2021-05-20T14:33:39.258Z"
  }
}

```
