---
title: 'Run Git Pull'
description: 'Run a git pull using the command API'
taxonomy:
    category:
        - docs
---

-

# Run a "git pull" command
The following sample script shows how you can run `git pull` commands in branch container...

```
$ cat run_command_in_a_running_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/6099303bc25947001370c/commands' \  'http://localhost:8080/api/sites/60993039c25947001370c/branches/6099303bc25947001370c/commands' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{ "commands": ["/bin/bash","-c","pwd && cd /var/www/html/web && git pull"] }' | jq -r ".result"
```
In the above code segmend, note how the following command is passed as three different arguments to the command API: `/bin/bash -c pwd && cd /var/www/html/web && git pull`

## Response

```
/var/www/html
Already up to date.
```
