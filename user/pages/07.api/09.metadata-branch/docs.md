---
title: 'Get Branch Metadata'
description: 'How to get branch metadata'
taxonomy:
    category:
        - docs
---

# Get Branch Metadata
The following sample script shows how you can get a branch's metadata...

```
$ cat get_meta_branch.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441-org"

curl -s -X GET \
  'https://api.site.devpanel.com/dev-v1/sites/60993039c25947001370c/branches/60993076fff9de0013e8d' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" | jq
```

## Response

```
{
  "_id": "60993076fff9de0013e8d",
  "isEnableEditor": true,
  "isEnablePMA": true,
  "lock": false,
  "doNotIndex": null,
  "secureConnection": false,
  "name": "develop",
  "branchType": "OTHER",
  "originBranch": "develop",
  "project": "60993039c25947001370c",
  "createdAt": "2021-05-10T13:09:10.302Z",
  "updatedAt": "2021-05-12T16:23:18.232Z",
  "appSettings": {
    "type": "drupal9",
    "adminUser": "devpanel",
    "adminPassword": "pwd2wus"
  },
  "applicationName": "develop-wscyl",
  "appserverImage": "wodby/php",
  "appserverTag": "7.4",
  "backupInterval": null,
  "copyDatabaseFilesApplication": null,
  "copyDatabaseFilesType": "HTTP",
  "copyDatabaseUrl": "s3://devpanel-cluster-vvybh/master-wscyl/dump/thkbnq7yjdc9h.tgz",
  "copyFilesUrl": "s3://devpanel-cluster-vvybh/master-wscyl/dump/h7zytg4u9ewpa.tgz",
  "customDomain": null,
  "dbName": "nameabc",
  "dbPassword": "nameabc",
  "dbUser": "nameabc",
  "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
  "deploymentConfig": {
    "application": {
      "storage": "20Gi"
    },
    "images": {
      "nginx": {
        "env": [
          {
            "name": "NGINX_SERVER_ROOT",
            "value": "/var/www/html/web"
          },
          {
            "name": "NGINX_VHOST_PRESET",
            "value": "drupal9"
          },
          {
            "name": "NGINX_BACKEND_HOST",
            "value": "0.0.0.0"
          },
          {
            "name": "NGINX_SET_REAL_IP_FROM",
            "value": "10.0.0.0/16"
          }
        ]
      },
      "php": {
        "env": [
          {
            "name": "APP_ROOT",
            "value": "/var/www/html/"
          },
          {
            "name": "WEB_ROOT",
            "value": "/var/www/html/web"
          }
        ]
      }
    }
  },
  "editorHostname": "cs-develop-wscyl.app.devpanel.com",
  "editorPwd": "devpanel",
  "environment": "5f68e1b2fb6a1a001c758",
  "fromApplicationName": "undefined-wscyl",
  "fromDB": null,
  "hostname": "dev-1370ca04-1370ca0a-13e8d.app.devpanel.com",
  "isEnablePolymorphing": false,
  "isEnablePolyscripting": false,
  "lockPassword": null,
  "lockUsername": null,
  "namespace": "devpanel-1620651997078jykm",
  "oldHostname": "dev-147b9a4f-147b9a55-147b9.app.devpanel.com",
  "pmaHostname": "pma-develop-wscyl.app.devpanel.com",
  "replicas": 1,
  "webserverImage": "wodby/nginx",
  "webserverTag": "1.17",
  "lastActivity": {
    "_id": "609c00a359ae0000181be",
    "action": "DEPLOY_APPLICATION",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "project": "60993039c25947001370c",
    "application": "60993076fff9de0013e8d",
    "environment": "5f68e1b2fb6a1a001c758",
    "status": "SUCCESS",
    "createdAt": "2021-05-12T16:21:55.783Z",
    "updatedAt": "2021-05-12T16:23:18.479Z",
    "lastResponse": {
      "id": "609c00a359ae0000181be",
      "status": "SUCCESS",
      "payload": {},
      "task": "DEPLOY_APPLICATION"
    },
    "taskName": null
  },
  "status": "DEPLOY_APPLICATION_SUCCESS",
  "customDomains": []
}
```
