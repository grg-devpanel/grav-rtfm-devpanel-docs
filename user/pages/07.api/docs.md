---
title: API Docs
description: Links to API Documentation
taxonomy:
    category:
        - docs
---

# Latest API Docs

## API Docs

### v1.0.556
[https://app.swaggerhub.com/apis-docs/johnnydevpanel/dev-panel_api/1.0.556](https://app.swaggerhub.com/apis-docs/johnnydevpanel/dev-panel_api/1.0.556)

### v1.0.554
[https://app.swaggerhub.com/apis-docs/johnnydevpanel/dev-panel_api/v1.0.554#/](https://app.swaggerhub.com/apis-docs/johnnydevpanel/dev-panel_api/v1.0.554#/)

# How to use the API
The following documentation describes how to use the API. We have created some simple scripts to demonstrate the use of the APIs... these scripts are for demonstration only. Create your own scripts for your purposes as needed.

!!!! We are aware that these scripts have hard coded values... they are there for demo only.

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.

## List examples
- [Login API](login): Start by logging in and getting a session token.
- [List Environments API](list-environments): List of Clusters/Insfrastructure that you have access to.
- [List Workspaces API](list-workspaces): List of workspaces you have access to.
- [List Sites API](list-sites): List of sites in a workspace.
- [List Branches API](list-branches): List of branches in a site.
- [List Commits API](list-commits): List of commits in a branch.
- [List People API](list-people): List of people in Workspace.
- [List Domains API](list-domains): List of domains in a Workspace.
- [List Backups API](list-backups): List of DB and Static Files Backups.

## Create examples
- [Create Workspace API](create-workspace): Create workspace for sites.
- [Create Site API](create-site): Create site in workspace.
- [Deploy Branch API](deploy-branch): Deploy code from branches.
- [Create Backup API](create-backup): Create Backups of DB and Static Files.
- [Add People API](add-people): Add People to Workspace.
- [Add Domain API](add-domain): Add Custom Domain to a Site Branch.
- [Create Cache API](create-cache): Create Cache for Site.

## Run commands

The `command` API lets you run arbitrary code in any branch's container... This is very handy as you can use it to do almost action you like on a branch using an API.

Here's is how the API is structured...
 ```
 https://api.site.devpanel.com/dev-v1/sites/<site-id>/branches/<branch-id>/commands' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{ "commands": ["arg1", "arg2", "etc."] }' | jq -r ".result"
 ```
In the above code segments, note that you need both `<site-id>` and `<branch-id>` for this API. Plus, note that you need to break up the command in an array as `"commands": ["arg1", "arg2", "etc."]`. The `std-out` that will be returned by this API will need to be parsed and formatted using the `jq` command as well.

- [Run Drush Commands](run-command): Runs Drupal drush commands.
- [Run Git Pull](run-gitpull): Run Git Pull.
- [Get ENV Vars](run-printenv): Get list of Environment Variables.
- [Run Composer Commands](run-composer): Run Composer commands.
- [Update Config](branch-reconfig): Update Branch Container Configuration.


## Get Logs/Stats
- [Get Site Metadata](metadata-site): Get metadata of a site.
- [Get Branch Metadata](metadata-branch): Get metadata of a branch.
- [Get PHP Log](log-php): Get PHP log of a branch.
- [Get NGINX Log](log-nginx): Get Nginx log of a branch.
- [Get ENV Vars](branch-env): Get ENV Variables of a branch.
- [Get Branch Stats](stats-branch): Get Hits of a branch.
- [Get Site Costs](org-costs): Get cost per site in org.

## Clear & Reset
- [Clear Cache](clear-cache): Clear CDN Cache for a branch.

