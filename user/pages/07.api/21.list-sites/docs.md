---
title: 'List Sites API'
description: 'List of sites in a workspace'
taxonomy:
    category:
        - docs
---

# List Sites API
Here's how you get a list of sites in a Workspace...

! The code and output below has been edited and sanitized and is therefore slightly different than actual input and output.


```
$ cat list_sites.sh
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="authorization: Bearer $JWT_TOKEN"

curl -s -X GET \
  https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370c/sites \
  -H "$AUTH_HEADER" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'auth-mode: cognito' \
  -H 'tenant: api-1620640441141-org' | jq 'map({ "_id": ._id, "name": .name })'
```

!!!! Here, the string `60992fddc25947001370c` is the Workspace ID.

When run, this script will generate the following results:

```
$ ./list_sites.sh
[
  {
    "_id": "60993039c25947001370c",
    "name": "Demo Drupal 9_1"
  }
]
```

Again, when run without `'map({ "_id": ._id, "name": .name })'`, you'll get a much more detailed response.

Here's the level of raw detail you can get on EACH site with this API:

```
$ ./list_sites.sh
[
  {
    "_id": "60993039c25947001370c",
    "tags": [
      {
        "_id": "6093bed752409d00130c0",
        "workspaces": [
          "6093bd33c80e5b0013cf9"
        ],
        "name": "Demo Drupal 9",
        "createdAt": "2021-05-06T10:03:03.300Z",
        "updatedAt": "2021-05-06T10:03:03.300Z"
      }
    ],
    "name": "Demo Drupal 9_1",
    "description": "Demo Drupal 9",
    "sourceCodeToken": null,
    "repositoryName": "may10th_drupal_9",
    "repositoryProvider": "GITHUB",
    "projectType": {
      "_id": "5f5600c4a19ceb6ef3d48",
      "name": "Drupal 9",
      "key": "drupal9",
      "order": 3,
      "initialRepo": "https://github.com/drupal/drupal",
      "preferredStack": "5cf673531c9d440000382",
      "stacks": [
        "5cf673531c9d440000382",
        "5d830bccd3201d97f6bcd",
        "5d83176ed3201d97f6be9"
      ]
    },
    "repositoryOwner": "johnnydevpanel",
    "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
    "workspace": "60992fddc25947001370c",
    "friendlyName": "wscyl",
    "repositoryId": "366046",
    "initialized": true,
    "createdAt": "2021-05-10T13:08:09.951Z",
    "updatedAt": "2021-05-12T16:21:57.587Z",
    "lastActivity": {
      "_id": "609c00a359ae0000181be",
      "action": "DEPLOY_APPLICATION",
      "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2b",
      "workspace": "60992fddc25947001370c",
      "project": "60993039c25947001370c",
      "application": "60993076fff9de0013e8d",
      "environment": "5f68e1b2fb6a1a001c758",
      "status": "SUCCESS",
      "createdAt": "2021-05-12T16:21:55.783Z",
      "updatedAt": "2021-05-12T16:23:18.479Z",
      "lastResponse": {
        "id": "609c00a359ae0000181be",
        "status": "SUCCESS",
        "payload": {},
        "task": "DEPLOY_APPLICATION"
      },
      "taskName": null
    },
    "repositoryWebHookId": "296599"
  },
```
