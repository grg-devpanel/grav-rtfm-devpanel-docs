---
title: 'Map Domain to Running Branch'
description: 'Map Domain to Running Branch'
taxonomy:
    category:
        - docs
---

# Map Domain to Running Branch
Map Domain to Running Branch

`cat map_domain_to_branch.sh`

```
#!/bin/bash
JWT_TOKEN=`cat JWT_TOKEN.txt`
AUTH_HEADER="Authorization: Bearer $JWT_TOKEN"
ORGANIZATION_HEADER="tenant: api-1620640441141-org"

curl -s -X POST \
  'https://api.site.devpanel.com/dev-v1/workspaces/60992fddc25947001370ca04/domains' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'tenant: api-1620640441141-org' \
  -H 'auth-mode: cognito' \
  -H "$AUTH_HEADER" \
  -H "$ORGANIZATION_HEADER" \
  --data-raw '{
    "privateKey":"",
    "sslCertificate":"",
    "domain":"my-domain-v1.app.devpanel.com",
    "application":"60993076fff9de0013e8df7d"
  }' | jq
```

## Response 

```
{
  "_id": "609c0404e265000013f0fd9c",
  "domain": "my-domain-v1.app.devpanel.com",
  "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
  "workspace": "60992fddc25947001370ca04",
  "application": {
    "_id": "60993076fff9de0013e8df7d",
    "isEnableEditor": true,
    "isEnablePMA": true,
    "lock": false,
    "doNotIndex": null,
    "secureConnection": false,
    "name": "develop",
    "branchType": "OTHER",
    "originBranch": "develop",
    "project": {
      "_id": "60993039c25947001370ca0a",
      "tags": [
        "6093bed752409d00130c0dc8"
      ],
      "name": "Demo Drupal 9_1",
      "description": "Demo Drupal 9",
      "sourceCodeToken": null,
      "repositoryName": "may10th_drupal_9",
      "repositoryProvider": "GITHUB",
      "projectType": "5f5600c4a19ceb6ef3d48584",
      "repositoryOwner": "johnnydevpanel",
      "createdBy": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
      "workspace": "60992fddc25947001370ca04",
      "friendlyName": "wscylanm",
      "repositoryId": "366046144",
      "initialized": true,
      "createdAt": "2021-05-10T13:08:09.951Z",
      "updatedAt": "2021-05-12T16:21:57.587Z",
      "lastActivity": "609c00a359ae0000181be303",
      "repositoryWebHookId": "296599473"
    },
    "createdAt": "2021-05-10T13:09:10.302Z",
    "updatedAt": "2021-05-12T16:23:18.232Z",
    "appSettings": {
      "type": "drupal9",
      "adminUser": "devpanel",
      "adminPassword": "pwd2wuszzluswuszzlus"
    },
    "applicationName": "develop-wscylanm",
    "appserverImage": "wodby/php",
    "appserverTag": "7.4",
    "backupInterval": null,
    "copyDatabaseFilesApplication": null,
    "copyDatabaseFilesType": "HTTP",
    "copyDatabaseUrl": "s3://devpanel-cluster-vvybhkeh/master-wscylanm/dump/thkbnq7yjdc9h0i2.tgz",
    "copyFilesUrl": "s3://devpanel-cluster-vvybhkeh/master-wscylanm/dump/h7zytg4u9ewpae8w.tgz",
    "customDomain": null,
    "dbName": "namewuszzlus",
    "dbPassword": "pwdwuszzlus",
    "dbUser": "userwuszzlus",
    "deployedBy": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
    "deploymentConfig": {
      "application": {
        "storage": "20Gi"
      },
      "images": {
        "nginx": {
          "env": [
            {
              "name": "NGINX_SERVER_ROOT",
              "value": "/var/www/html/web"
            },
            {
              "name": "NGINX_VHOST_PRESET",
              "value": "drupal9"
            },
            {
              "name": "NGINX_BACKEND_HOST",
              "value": "0.0.0.0"
            },
            {
              "name": "NGINX_SET_REAL_IP_FROM",
              "value": "10.0.0.0/16"
            }
          ]
        },
        "php": {
          "env": [
            {
              "name": "APP_ROOT",
              "value": "/var/www/html/"
            },
            {
              "name": "WEB_ROOT",
              "value": "/var/www/html/web"
            }
          ]
        }
      }
    },
    "editorHostname": "cs-develop-wscylanm.app.devpanel.com",
    "editorPwd": "devpanel",
    "environment": "5f68e1b2fb6a1a001c758684",
    "fromApplicationName": "undefined-wscylanm",
    "fromDB": null,
    "hostname": "dev-1370ca04-1370ca0a-13e8df7d.app.devpanel.com",
    "isEnablePolymorphing": false,
    "isEnablePolyscripting": false,
    "lockPassword": null,
    "lockUsername": null,
    "namespace": "devpanel-1620651997078jykmxih",
    "oldHostname": "dev-147b9a4f-147b9a55-147b9a58.app.devpanel.com",
    "pmaHostname": "pma-develop-wscylanm.app.devpanel.com",
    "replicas": 1,
    "webserverImage": "wodby/nginx",
    "webserverTag": "1.17",
    "lastActivity": "609c00a359ae0000181be303",
    "status": "DEPLOY_APPLICATION_SUCCESS"
  },
  "privateKey": "",
  "sslCertificate": "",
  "createdAt": "2021-05-12T16:36:20.232Z",
  "updatedAt": "2021-05-12T16:36:20.232Z",
  "createdByInfo": {
    "sub": "95a3b391-5bdb-4f46-8931-2ab35bf2bae0",
    "email_verified": "true",
    "email": "huytran+2@webenabled.com",
    "role": "INSTANCE_ADMIN",
    "status": "ACTIVE",
    "user_metadata": {
      "githubAccessToken": "gho_nTQOIWRwwIuUGeIPKRQ"
    }
  }
}
```
