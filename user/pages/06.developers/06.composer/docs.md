---
title: Composer
description: Runnning Composer for Drupal apps
taxonomy:
    category:
        - docs
---

# Composer 1 vs. 2

In VSCode, both Composer 1 and Composer 2 are currently available. Composer 2 can be run as ``composer`` in the terminal, whereas Composer 2 is available as ``composer1``.

# How to let Composer install a version that is not stable, but rather alpha

Run these two commands

```
composer config minimum-stability alpha
composer config prefer-stable true
```

The first one allows installing alpha packages. Available options (in order of stability) are dev, alpha, beta, RC, and stable.

The second one instructs Composer to prefer stable packages when they are available.

More details here: https://getcomposer.org/doc/04-schema.md#minimum-stability

# How to run Composer with unlimited memory

Set the shell environment variable `COMPOSER_MEMORY_LIMIT` to `-1`:

```
COMPOSER_MEMORY_LIMIT=-1 composer install <PACKAGE>
```
