---
title: Solr
description: Setting up the Solr search engine for Drupal
taxonomy:
    category:
        - docs
---

# Creating a Solr instance for your application

Creating a Solr instance is similar to creating a Memcached instance. Please follow the same steps as described on the [Memcached page](/developers/memcached), but choose Solr instead of Memcached.

Once the Solr instance is created, expand its "Instance  information" in the "Resources" tab in the same way as it is done for Memcached:

![01-solr.png](01-solr.png)

You will need the following values for copy/pasting them to Drupal's Search API configuration dialog in the next section:
1. host
2. port (usually 8983)
3. username (usually "devpanel")
4. password

# Configuring Drupal to use the Solr instance as a search engine

1. Open VSCode:
![04-memcached-vscode.png](../memcached/04-memcached-vscode.png)

2. In VSCode, open a new terminal and install the drupal/search_api_solr module and its dependencies by typing the following command:
```
composer require symfony/event-dispatcher:~3.4.0 solarium/solarium:^6.1 drupal/search_api_solr:^4.2
```
3. Setup Search API
Navigate to Drupal Admin Panel > Configuration > Search and metadata > Search API
  1. Click + Add server.
  2. In Backend options, choose Solr
  3. In CONFIGURE SOLR BACKEND, enter the information below    
   > - Solr Connector: Basic Auth
   > - Solr host: SOLR_HOST_GENERATED_BY_DEVPANEL
   > - Solr port: 8983
   > - Solr path: /
   > - Solr core: devpanel
   > - Username: devpanel
   > - Password:  SOLR_PASSWORD_GENERATED_BY_DEVPANEL
  4. Save and create your index
 
!!!! On step 3.3, you will need "Instance information" retrieved at the end of the previous section.

  
