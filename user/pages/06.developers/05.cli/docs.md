---
title: DevPanel CLI
description: DevPanel CLI repository on Github
taxonomy:
    category:
        - docs
---

See this external link: [DevPanel CLI repository on Github](https://github.com/devpanel/cli/tree/develop)

!! The repository is currently private
