---
title: Drupal
description: Drupal Setup and Config
taxonomy:
    category:
        - docs
---

# Drupal Admin Access

Using the built-in VS Code IDE, use the following `drush` command to get a one-time admin access link:

```
drush uli -l $DEVPANEL_HOSTNAME
```

Notice the difference between using this `drush` command with and without arguments:

![drush_uli_2021-11-25_11-46-47.png](drush_uli_2021-11-25_11-46-47.png)

1. without arguments: the login link that's generated begins with `https://default` which will not work since the URL of the site is different
2. with argument: the login link begins with the correct domain name... all you have to do is "Control+Click" on this URL to login to your Drupal site as an admin.

# Configuring a Drupal Multi-Site

There are many ways to set up a Drupal multi-site in DevPanel, but the most simple, easy and clean approach is to create a separate DevPanel "Project" for each of your multi-sites using the following process:

## Verify that your repo is set up correctly
In your repo, you should see each of your (multi-site) site in a separate directory under the \<docroot\>/sites/ directory as such:

![a_2021-11-02_15-00-53.png](a_2021-11-02_15-00-53.png)


## Set up separate projects for each site in DevPanel

![b_2021-11-02_14-54-44.png](b_2021-11-02_14-54-44.png)

In DevPanel, 
1. On the Projects tab of your Workspace
2. Create a separate project for each (multi-site) site
3. Name each project the name of that (multi-site) site
4. Point each project to the same repo

Start with just the master/main branch. Build out the application on the master/main branch in each project using the database for that (multi-site) site... or build it from scratch and then import the (multi-site) site database into that application through PhpMyAdmin replacing the original database. 

## Create a `default` sym-link in each project

![c_2021-11-02_10-53-48.png](c_2021-11-02_10-53-48.png)

Use the following command to create the sym-link:

```
cd <docroot>/sites
ln -s $PWD/<multi-site dir> default
```

!!!! The `default` directory in each project will point to it's own multi-site

!!!! JFYI: sym-links are also known as symbolic-links and soft-links

!!!! You might want to put the default directory in git-ignore so it stays out of your repo. Since each project will be using the same source tree, and each project will have a different default directory, it makes no sense to have the default directory in the source tree... and having it there might cause some issues too.

## Test everything


Test the application URL of the master/main branch of each project to make sure the correct (multi-site) site comes up. 

Create a new `develop` or `feature` branch in your repo. Once you do this, these brances will show up in all the projects built from that repo. 

Building out that branch in each project as a clone of master/main in that project should work correctly... 

!!!! The cloning in project is based on the `default` folder/pointer within each branch.


