---
title: Custom Domains
description: How to assign a custom domain to your application
taxonomy:
    category:
        - docs
---

# Assign a Domain Name

First, create an application and make get it ready to publish it under a custom domain name.

! Make sure the application is secure and you're using strong passwords.

## Select an application

In this example, we're going to use the following application:

![2021-03-02_18-06-52_custom_domain_1a.png](2021-03-02_18-06-52_custom_domain_1a.png)

We will assign a custom domain to the "MASTER" branch of the "Drupal 8" application.

## Configure domain name

Go to the "Custom Domains" page of the workspace 

![2021-03-02_18-31-02_new_custom_domain_1a.png](2021-03-02_18-31-02_new_custom_domain_1a.png)

Next enter your domain information...

![2021-03-02_18-09-57_custom_domain_2a.png](2021-03-02_18-09-57_custom_domain_2a.png)

1. Enter the domain name (proceeded by a sub domain, like www. or my.)
2. Select the Project from the dropdown (eg: Drupal 8)
3. Select the Branch of that project (eg: Master)

!!!! COPY this CNAME string and save it some safe place.

Configure your DNS to point to this "CNAME" - this should be done with your Domain Name Registrar (like Godaddy.)

After you click the "Save" button, you will see the following screen.

![2021-03-02_18-11-07_custom_domain_4a.png](2021-03-02_18-11-07_custom_domain_4a.png)

## SSL Certificate

You will need to upload your own SSL Certificate for this domain. 

You can upload a purchased SSL Certificate or generate a free one with LetsEncrypt. 

! If you use a LetsEncrypt SSL Certificate, you'll have to renew/replace/rotate the cert yourself.

