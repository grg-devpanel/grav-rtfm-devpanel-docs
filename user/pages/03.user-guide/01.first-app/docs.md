---
title: First Application
description: How to create your first application. Example: Drupal 8 from scratch.
taxonomy:
    category:
        - docs
---

In this example, we will create our first application, which will be Drupal 8 from scratch. For simplicity, we will use the defaults proposed by devpanel.com where possible.

# Creating a new project

!!!! Project connects DevPanel to ONE Git repository
!!!! In the project, you can create many applications. One application for each branch or tag in that repository.

A workspace can have many projects.

To create a project, click the "New Project" button.

![new-project-01.png](new-project-01.png)

## Basic information
Enter the project name, description, click on "Next".
![new-project-02-basic-information.png](new-project-02-basic-information.png)

## Source code: application type and repository  for storing the code
Select the application type and link to your source repo.

![new-project-03-source-code.png](new-project-03-source-code.png)
 
##  Linking your git provider account to the new project

![new-project-04-link-your-github-account.png](new-project-04-link-your-github-account.png)

Enter your credentials in the pop-up window shown below.

!!!! If you are already logged in to GitHub, this operation will be performed automatically.

![new-project-05-login-to-github.png](new-project-05-login-to-github.png)

## Selecting a repository
![new-project-06-new-repository.png](new-project-06-new-repository.png)

Next, you can select between "Create new repository" or "Use existing Repository".

To create a new app, click on "Create new repository", enter the name of the new repo and click Next.

![new-project-07-repository-name.png](new-project-07-repository-name.png)

## Review the "Source Code" form
Make sure all the marked items are correct and what you want...

![new-project-08-source-code-summary.png](new-project-08-source-code-summary.png)

## Confirm and launch
![new-project-09-review-and-launch.png](new-project-09-review-and-launch.png)

This is the last chance to review the details of the new project. If you are unsure, you can click on "Back" and review the details you had entered previously.

If everything is OK, click on "Confirm and Create Project", and this will initiate the project creation procedure.

This process creates a new repo and loads it up with source code... so it can take a few minutes.

## Project creation in progress
You will see a blue window with the log describing various steps of the project creation process.

![new-project-10-in-progress.png](new-project-10-in-progress.png)

# Creating an application by deploying a branch

To create an application using the source code of one of the repository branches, select the branch to deploy ("master" by default), leave the default source of database and static files ("from scratch") and click "Next".

![new-app-01-select-branch-database-files.png](new-app-01-select-branch-database-files.png)

## Configure security

Just accept the defaults by clicking "Next".

![new-app-02-configure-security-defaults.png](new-app-02-configure-security-defaults.png)

## Configure settings

Again, accept the defaults by clicking "Next".

![new-app-03-configure-settings-defaults.png](new-app-03-configure-settings-defaults.png)

## Review and launch

Initiate the branch deployment. This process will create the new appliccation in the kubernetes cluster and can take several minutes.

![new-app-04-review-and-launch.png](new-app-04-review-and-launch.png)

## Deployment in progress

As with project creation, we can see a blue window with the logs describing various steps of application creation.

![new-app-05-in-progress.png](new-app-05-in-progress.png)

# The new application is ready to use

![new-app-06-ready.png](new-app-06-ready.png)

Open the new drupal site by clicking on the application public URL (1) and login to drupal by using the admin's username (2) and password (3) which can be copied to the clickboard.

Additionally, you can open the tools PHPMyAdmin (4) and VSCode (5).

## Login to Drupal 

![new-app-07-drupal-login.png](new-app-07-drupal-login.png)

## PHPMyAdmin

Database editor.

![new-app-08-phpmyadmin.png](new-app-08-phpmyadmin.png)

## VSCode

Visual Studio Code editor with a bultin terminal for running the Linux shell.

![new-app-09-vscode.png](new-app-09-vscode.png)

# Deleting the application by undeploying the branch
Select "Danger zone".

![new-app-delete-01-danger-zone.png](new-app-delete-01-danger-zone.png)

Type the branch name to be undeployed and click on "Delete Application".


![new-app-delete-02-branch-name.png](new-app-delete-02-branch-name.png)

! For safety reasons, you are required to type the branch name manually to prevent accidental deletion.

## Application deletion (branch undeployment) in progress

![new-app-delete-03-in-progress.png](new-app-delete-03-in-progress.png)

## Deleting the project

! Deleting the project is only possible when all deployed applications are deleted

Click on "Settings"

![new-project-delete-01-settings.png](new-project-delete-01-settings.png)

Scroll down to the "Danger zone", type "DELETE" and click on "Delete Project".

![new-project-delete-02-danger-zone.png](new-project-delete-02-danger-zone.png)

! For safety reasons, you are required to type the word "DELETE" manually to prevent accidental deletion.

# Conclusion

In this article, we learned how to:
1. Create a project
1. Create an application in the new project
1. Use the application and additional tools (PHPMyAdmin and VSCode)
1. Delete the application
1. Delete the project
