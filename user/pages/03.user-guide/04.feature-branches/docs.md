---
title: Feature Branches
description: How to create feature branches and test apps.
taxonomy:
    category:
        - docs
---

# Creating Branches
To create a branch of an application, first go to the repo of that application. 

Click on the repo name on the application page.

![2021-03-01_19-35-29_go_to_feature_branch.png](2021-03-01_19-35-29_go_to_feature_branch.png)

this link will open the repo page 

---

In GitHub (or in your git provider,) create a new branch...

![2021-03-01_19-29-50_create_feature_branch.png](2021-03-01_19-29-50_create_feature_branch.png)

Verify that the branch is created in GitHub (or your git provider)

![2021-03-01_19-39-39_feature_branch_created.png](2021-03-01_19-39-39_feature_branch_created.png)

When you return to DevPanel, you will see the new branch under "Branches" view.  If you do not see the new branch, click the "Sync All Branches" button and REFRESH the page.

![2021-03-01_19-45-02_feature_branch_on_devpanel.png](2021-03-01_19-45-02_feature_branch_on_devpanel.png)

From here, you can now build out this branch into an application.
