---
title: Navigating DevPanel
description: Got your app, now what? see how you can get around...
taxonomy:
    category:
        - docs
---

# Application Page
## "Overview" tab
Here's what you can expect to see once your application is built out...

![navigating-devpanel-01-application-overview.png](navigating-devpanel-01-application-overview.png)

In the Workspace tab, here are some page elements you should be aware of...

1. Navigation Breadcrumbs 
1. Project Name
1. Repo Name and Link
1. Project Navigation
1. Navigation between branches
1. Operations on the current branch
1. Application Public URL
Use it to open the application (web site)
1. Development Tools
1. Admin Account Credentials
1. Activities: Build History and Logs
When clicked, you will see something like that:
![navigating-devpanel-03-application-activities.png](navigating-devpanel-03-application-activities.png)
This history will grow when you perform new operations on the branch.


!!!! Menu selections may vary depending on the version of DevPanel you're on and depending on your level of access.

## Notes

If you are on a "Testing Cluster," then your application will automatically be deleted after (n) hours. 

You can also "PAUSE" or suspend applications to save system resources. 

Update Config button rebuilds the container configurations. This is useful if you want to allocate more memory to your application.

! Unlike the previous screenshot with the Drupal application, this one illustrates a Wordpress application, and the "Admin account" credentials are missing: they are to be selected manually during Wordpress setup.

![2021-02-26_14-27-36_navigating_devpanel_3.png](2021-02-26_14-27-36_navigating_devpanel_3.png)

## "Code" tab
Contains direct links to the pages of your git provider 

![navigating-devpanel-02-application-code.png](navigating-devpanel-02-application-code.png)

## "Database/Files" tab

![navigating-devpanel-04-application-database-files.png](navigating-devpanel-04-application-database-files.png)

1. Export database
When clicked, a download link will appear to a file containing the SQL dump of the application database.
2. Export files
When clicked, a download link will appear to a compressed archive of static files.

## "Logs" tab

![navigating-devpanel-05-application-logs.png](navigating-devpanel-05-application-logs.png)

Use the "Logs" tab for looking at the logs of your application. 

!!!! Standard devpanel applications usually run in two docker containers: PHP and Nginx. You can select the logs of the container you need by selecting log type, marked (1) on the screenshot.

! Application logs are produced by working applications and are different from the logs produced when creating those applications or performing various operations on them. These logs can be opened using the link marked (2) under the "Activities" section on the right of the page. 

## "Static sites" tab

! Add explanation

![navigating-devpanel-application-06-static-sites.png](navigating-devpanel-application-06-static-sites.png)

## "Security" tab

Use this tab to lock the application by enabling HTTP auth. This can be used, for example, to prevent public access to a site under construction. An attempt to open such a site will make the browser ask for username and password.

![navigating-devpanel-07-application-security.png](navigating-devpanel-07-application-security.png)

1. Turn the lock on.
1. Enter the username.
1. Enter the password.
1. Click on "Update Application". The update can take  up to one minute.

! The change will not take effect until "Update application" is clicked.

!!!! When the site is ready for public access, turn off the lock switch marked (1) and do not forget to click on "Update application" (4) again.

## "Danger zone" tab (delete application)

In order to delete the application, select "Danger zone", type the branch name to be undeployed and click on "Delete Application".

![new-app-delete-02-branch-name.png](new-app-delete-02-branch-name.png)

! For safety reasons, you are required to type the branch name manually to prevent accidental deletion.

### Application deletion (branch undeployment) in progress

The deletion process can take more the one minute.

![new-app-delete-03-in-progress.png](new-app-delete-03-in-progress.png)
