---
title: HELM Charts
description: 
taxonomy:
    category:
        - docs
---

Kubernetes deployments are created with HELM Charts

On the Deployments page, you'll be able to select which HELM Chart you want to use...

Once you select the HELM Chart and the version of the chart, you'll next set the deployment details by filling out the chart values

![](helm-chart-and-main-image-2021_6h1jxm.png)

!!!! Each HELM Chart is discussed in more detail in other articles
