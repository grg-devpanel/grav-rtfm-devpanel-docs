---
title: How Deployments Work
description: Managing Application and Kubernetes Deployments
taxonomy:
    category:
        - docs
---

# About Deployments

In Kubernetes, a "deployment" consists of (n) containers of an application. 

!!!! This is an oversimplified description of a deployment... check online for a more detailed description.
!!!! For a deployment to work, we first have to build a container "image" with the application. Kubernetes then deploys many copies of this container image in "pods" inside a cluster. 

The number of "replicas" refers to the number of pods running at any given time. For example, if you tell Kubernetes to maintain a replica set of 4 for your application, then Kubernetes will run 4 pods for you at all times. If a pod dies, then that pod is discarded and a new pod is created. 

If we have auto-scaling enabled, the number of pods will go up and down depending on the load and traffic to your applications / website. 

# Deployments vs Applications

In DevPanel, Deployments are built from Applications. 

You can configure your environment so a tag-based release of your Application will create a new Deployment of that Application. 

Deployments are "immutable" and versioned, so if you create accidentally create a "bad deployment," you can always "roll-back" the deployment to a previous version.

# Dependencies

Before you create Deployments, make sure to do the following:

1. Create and test your application. It's what we'll deploy.
2. Create application database secrets for connecting to DB
3. Create Container Registry "Pull Secrets" for pulling images

Application database secrets let you deploy an application in different environments.... For example, these will create environment variable sets for Dev, Test, and Live that will let you deploy the same app into Dev, Test or Live environments depending on the variable set you use.

Container Register secrets lets DevPanel connect to your container registry on your behalf.
