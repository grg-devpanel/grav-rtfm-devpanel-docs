---
title: Creating Deployments
description: There are many steps to creating a deployment. We will walk you through them here
taxonomy:
    category:
        - docs
---

# Start with an Application

Create your application first... here, we're starting with a Drupal 8 application.

![01-start-with-an-application-2_1pd9ei0.png](01-start-with-an-application-2_1pd9ei0.png)

Customize it to your liking, and make sure it works... 

![](02-test-your-app-2021-03-2822-_zbjxfy.png)

# Setup Secrets

## Create a Secret Set

Go to the Secret Manager and create a secret set

![](03-create-secret-set-2021-03-2_1rq8nvb.png)

## Gather Secrets

Go through your application and get all the key, value secret pairs that your application is using... for example:

![](04-gather-secrets-2021-03-2823_1nttmq.png)

## Transfer Secrets to the Secret Set

![](05-transfer-secrets-2021-03-28_1my498h.png)

# Create a Deployment

![](06-create-deployment-2021-03-2_85ptrc.png)

1. Deployment Name should be unique. Leave namespace to "Default"
2. Set Go Live Policy to Automatic or Manual. Manual is safer and lets you preview new builds before rolling them out. With Automatic, new releases that match the tag are automatically rolled out.
3. Tag Pattern for matching. You can set up many named deployments with each watching a different tag pattern. If your application release on the registry matches this pattern, then that release is considered part of this deployment. 
4. Secret Variable Set. Select the set from the drop-down and verify that the variables you're planning on using appear in the set.
5. There are many HELM charts to choose from, but for auto-scaling apps, you'll want to use the "basic-hpa." HPA stands for Horizontal Pod Auto-scaler.
6. Pick the latest version - unless there's a problem with your app deployment and you want to try a previous version of the HELM chart.

# HELM Charts

Provide the values for HELM Chart that will be used to create the deployment.

!!!! The number of questions will vary depending on the HELM chart you chose, 

## Container register info

![](07-helm-values-2021-03-2823-15_m92rjp.png)

## Pull Policy and Ports

![](08-pull-policy-and-ports-2021-_f22y9f.png)

## Environment Variables

![](09-env-vars-2021-03-2823-18-23_1vz2bm3.png)

!!!! The "CODES_ENABLE" variable controls inclusion of VS CODE (CODE SERVER) into the deployment pods... For production deployments, you should set this to "no"; for dev and test deployments, you may set this to "yes"

## Pod Resources

![](10-pod-resources-2021-03-2823-_91wdc3.png)

## Optional Parameters

Specify Volume Mounts, Init Containers, Sidecars, and Image Pull Secrets here...

![](11-optional-parameters-2021-03_40d63t.png)

## Deployment Strategy

Pick the deployment strategy you wish to use... you can select "Rolling Update" or "Recreate"; These are both standard Kubernetes deployment strategies - more info online.

![](12-deployment-strategy-2021-03_k3015g.png)

! The system, by default, does not check for Liveness and Readiness - you need to configure your application to do this if you want this.

![](13-liveness-readiness-2021-03-_15ztmgp.png)

## Persistent Volumes

Select "AWS EBS GP2" if you want Persistent Volumes in your pods... Selecting "EmptyDir" will flush the data as pods are destroyed.

![](14-persistent-volumes-2021-03-_aijdx7.png)

## Horizontal Pod Autoscaler

![](15-hpa-2021-03-2823-36-49_ph5ym.png)

Here's a brief description for each parameter:
* Max Replicas: the maximum number of pods that autoscaler is allowed to scale up to.
* CPU Average: the CPU threshold at which new pods will be created. If this is set to 60, for example, then when the average CPU usage of the cluster reaches 60%, the HPA will start creating new pods. 
* Memory Average: like CPU threshold, this is the Memory threshold. 

# Save your work

If you like, you can copy all the information you provided to the clipboard - to save it offline. 

Click Update to save the form. This data will be used to generate the HELM charts for your deployments.

![](17-save-2021-03-2823-36-49_hikmpk.png)

On the Deployments page, this is how the newly created deployment shows up...

![](18-new-deployment-2021-03-2823_4hm7hw.png)
