---
title: Linking Container Registry
description: 
taxonomy:
    category:
        - docs
---

# Deployment Tokens

In your git provider, GitLab for instance, create a Deployment Token...


![](git-create-deploy-token-2021-0_4teva4.png)

# Create Secrets

Copy the data from your git provider into the Container Registry Secrets fields

![](create-container-registry-secr_4knqmz.png)

Save this data to generate the Secret key

![](create-new-container-registry-_1ikz1s3.png)

Copy this key - we'll use it in the HPA Deployment HELM Chart

# HELM Chart - Image Pull Secrets

In the HELM Chart, paste this key under Image Pull Secrets...

![](helm-image-pull-secrets-2021-0_1bxbfv4.png)

This lets the HELM Chart connect to your Container Registry to pull images.

