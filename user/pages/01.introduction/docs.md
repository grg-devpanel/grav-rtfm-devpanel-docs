---
title: Introduction
description: 'Introduction to DevPanel'
taxonomy:
    category:
        - docs
---

# What is DevPanel

DevPanel is a point-and-click control panel that automates setup, configuration, deployment and hosting of applications on AWS. 


“What took us four people and three months, DevPanel did in 17 minutes!” said one government contractor. “We wish we had found DevPanel sooner” they continued.


# Benefits


DevPanel is a toolbox that instantly benefits your teams and organization.


Top 10 benefits of using DevPanel (as cited by customers):

1. Deploys legacy and new versions of Drupal within 5 minutes.
2. Creates fully functional Cloud Based Dev Environments with IDE within 5 minutes.
3. Increases development velocity and deployment speed by at least 2x.
4. Makes Developers and SysAdmins 2-3x more efficient with self-service portal.
5. Eliminates mistakes on site and infrastructure setup by 80% using automation.
6. Save 1-2 hours / day / developer on active projects.
7. Add and remove developers / contractors with 2 clicks.
8. Know who did what on which projects within 5 minutes.
9. Discover, debug and resolve problems 50% faster.
10. Save up to 80% in “hard” and “soft” costs.


# Features

1. Auto-scaling container-based “serverless” compute infrastructure
   - No servers to manage!
   - No packages to upgrade!
   - Fully Cloud-Native Infrastructure
2. Built-in DNS management
   - Custom domain management 
   - Amazon Route 53
3. Built-in SSL Cert management
   - Amazon Certificate Manager (ACM)
   - LetsEncrypt
4. Create and manage Amazon CloudFront CDN
   - With built-in one-click CDN management
   - DDOS protection with built-in Standard AWS Shield 
   - And custom CDN setup and configuration 
5. Connect with AWS WAF
   - Create, configure and monitor custom rules
6. Configuring backup and archive jobs
   - Backup and Restore any sites (feature, dev, test, prod, etc.)
   - Create manual snapshots as needed (Code, DB, Files)
   - Enable automated scheduled backups 
   - One-click on-demand restore
7. Sync any site to any site
   - From prod site to dev and test sites
   - From one dev site to another dev site
   - With code, files, and databases
8. Integrate with any/all 3rd party and AWS tools
   - New Relic APM, Splunk, DataDog, Pinpoint, etc.
   - AWS WAF, Sucuri, F5, Barracuda, etc.
   - AWS CloudFront CDN, Cloudflare, Akamai, Fastly, etc.
   - AWS Cloudwatch, PagerDuty, etc.
9. One-click deployments
   - Branch-based and Tag-based deployments
   - Blue/Green deployments (for rollbacks)
   - Multi-container & Multi-AZ deployments (for high-availability)
   - Select SPOT or On-Demand compute to optimize cost savings
10. Unlimited development environments
   - With built-in Cloud Based Development Environment
      - like local dev environments 
      - but with secure browser-based VS-Code IDE and PhpMyAdmin
      - on-board developers in minutes not days
   - Works w/ all local development environment tools 
      - Eg: DDEV, Lando, Docksal, etc.
   - Pre-integrated with Github, Gitlab, and BitBucket
   - Ability to invite and collaborate with internal and external team members
   - Create secure AWS resources as needed with few clicks….
      - Eg: Memcached, ElasticSearch, S3 Buckets
      - Eg: CloudFront CDN with Standard AWS Shield
      - Eg: SSL Certs on Amazon Certificate Manager (ACM)
11. Built for secure and stable DevOps processes
   - Built with AWS-managed components to maximize reliability and uptime with minimal manual intervention
   - Uses automation to apply security rules on creation of sites and resources
   - Support for multiple development methodologies 
      - like Gitflow, Trunk based development, etc.
      - Eg: separate branches for each feature
      - Eg: separate branches for each story
      - Eg: separate branches for each developer
      - Eg: single integration and testing branch
   - Support for multiple release types
      - Branch-based releases
      - Tag based releases
   - Secrets management
   - Support for multiple deployment types
      - Rolling deployments
      - Blue/Green deployments
   - Full CI/CD support
      - Github Actions
      - Gitlab Runners
      - AWS CodeStar, Bitbucket Pipelines
      - Jenkins, Travis, CircleCI, etc.
12. Fully “Cloud Native” Stack
   - Kubernetes based hosting stack
   - Flexible, modular, and extensible
   - Integrate with components from the CNCF landscape
     !!!! See [https://landscape.cncf.io/](https://landscape.cncf.io/)
13. Zero lock-in. 
   - Your site can run in your own AWS account
   - You have full root level access
   - Your sites keep running if and when you disconnect DevPanel


# Security:

We keep all components of the tech stack up-to-date, regularly backed up, and patched against any security vulnerabilities. We operate in a shared security model, similar to that of AWS. In this model, 
   * AWS provides physical security for the infrastructure and security for AWS managed services used in the hosting stack. 
   * We, in cooperation with our clients, configure ingress, egress, security groups, WAF rules, S3 access rules, CDN, DDOS protection, access monitoring, log aggregation and monitoring, and security alerts. 


Some notable features of the hosting environments are as follows:
   * HIPAA compatible infrastructure access (with AWS) 
   * Fully customizable ingress, egress, security groups and WAF rules
   * Serverless Drupal Compute Stack with no (pet) servers to manage or patch
   * AWS managed components: Kubernetes Control Plane, Fargate Compute, Elastic File System, S3, CDN, AWS Shield (DDOS Protection), ElastiCache, Elasticsearch, Route 53, ACM, RDS Aurora, ALB, etc.
   * Full root-level access to your sites and infrastructure for compliance audits (optional)
   * Fully-isolated instance of DevPanel control panel with VPN access (optional)


# Monitoring and Alerts (optional):
   * Prometheus and Grafana on user environment
   * Integration with Pingdom, PagerDuty, Splunk, etc.


# Disaster Recovery (optional):
   * Cross-account and cross-region Disaster Recovery (DR) implementation
   * Multi-Regional Highly-available and fault-tolerant infrastructure


# Customization (optional):
   * Advanced DDOS protection
   * Client-specific Terraform scripts and custom infrastructure
   * Multi-AZ and Multi-Regional implementations with custom failover rules
   * Fully managed custom hosting stacks 

---

# Overview of our company

DevPanel is primarily a technology company. Our mission and objective is to provide the tools to
make developers, sysadmins and project managers 2-3x more productive, implementation 5x faster, and
maintenance 10x cheaper. We do this by providing a self-service portal that automates 90% of the
activities that most developers, sysadmins and project managers do out of the box. The rest we
customize during implementation. 

For implementations, we generally work closely with client’s internal team and/or client’s vendors
who will maintain and support the applications post implementation. Where clients don’t have a
dedicated team for an implementation project, we work with Implementation Partners and/or MSPs
(Managed Service Providers) to support the client’s needs for the duration of the project. 


# Our experience in hosting Drupal websites

Technically, we don’t host any websites. We provide tools for development, deployment and management
of websites including creation and management of infrastructure for deployments in our client’s AWS
accounts. The hosting is done by AWS using AWS managed components. 


DevPanel has been used to create both small and large websites alike. It has been used to create and
deploy numerous “template” websites as well as static websites. 

Templates are often used by associations with thousands of clubs and schools with hundreds of
departments, each with similar site structures, headers and footers and branding and legal
disclosure mandates – see testimonials.

On the large site front, DevPanel has been used to develop and deploy sites that handle billions of
requests per month. Some users have saved up to 80% on their hosting costs by moving to AWS using
DevPanel – see testimonials.

Alternatively, some clients are consolidating their sites from multiple Drupal PaaS hosting
providers to AWS using DevPanel to provide consistency and to streamline their operations and
support – see testimonials. 

Although many companies use DevPanel to manage custom applications (built using Node JS, Python,
etc.) we offer out-of-box support for Drupal 7, Drupal 9 and Backdrop. 


# Enterprise projects

We conduct a discovery session (aka scoping session) at the beginning of the project to define the
scope of the project, outline the requirement, document the timeline, assign team members, and
estimate effort and costs for the project. 

# Details about our team


Our team consists only of senior developers, sysadmins and project managers… 

As mentioned above, for implementations, we generally work closely with client’s internal team
and/or client’s vendors who will maintain and support the applications post implementation. Where
clients don’t have a dedicated team for an implementation project, we work with our Implementation
Partners or MSPs (Managed Service Providers) to support the client’s needs for the duration of the
project. 


We build implementation teams with the appropriate staff at the scoping session based on project
requirements and expertise gap. Depending on the need, some members might only join the team for a
day or two while others may stay with the team throughout the project. 


Implementation teams may consist of following roles:
   * Account Executives
   * Project Managers
   * Technical Advisors & Architects
   * Sr. Developers & SysAdmins
   * AWS Solutions Architects
   * AWS Technical Security Architects
   * AWS Network Security Architects

# Timeline for Enterprise implementation

Implementation projects range from 1 - 3 months. Firm time commitments are provided after the scoping session.

# Hosting examples / case studies 

Example:
- Actual site: [https://ahead.hiv.gov/](https://ahead.hiv.gov/)
- Live dev/uat site (may be locked or down at times depending on development activities):
  [http://prod-144c3b8e-9a9d75c5-7cb74f22.app.devpanel.com/](http://prod-144c3b8e-9a9d75c5-7cb74f22.app.devpanel.com/)

Case Study: 
- Voice of America (VOA) - see 
[https://drive.google.com/file/d/12_1Tb7CEeliHJyHrqoyPA1qjGS0691E0/view?usp=drivesdk](https://drive.google.com/file/d/12_1Tb7CEeliHJyHrqoyPA1qjGS0691E0/view?usp=drivesdk)


# References and/or testimonials from current or past customers

“We wouldn’t trust anyone else with our business and website!” - R.M.


“DevPanel is the AWS partner that we never had before. Their tools are like training wheels for AWS
and their support is like having our own in-house DevOps team.” - E.B.


“I would have never tried AWS if it wasn’t for DevPanel. Now I feel like an expert - even though I
know I’m not.” - D.V.


“It was terrible in the beginning! Now, we don’t even notice it. Everything just works. We’ve stuck
with it from its inception because of the amazing team.” - E.C.


“From $1m a year to $128k a year. What more is there to say?” - J.L.


“What took us four people and three months, DevPanel did in 17 minutes!” – L.C.


“I wish we had found DevPanel sooner. We could have cut months from our project.” - L.M.


“It’s the only system we’ve found that lets us consolidate all our sites from all the different
hosting vendors into one consolidated platform.” - B.R.


“With Cloud Based Dev Environments, we can spin up Drupal dev sites in less than 5 minutes and
onboard new contractors in less than one hour! This same thing used to take us at least 2 days [to
set up] before DevPanel.” - C.M.


“Managing security and off boarding developers has never been easier. There are no ssh keys to
manage and to servers to clean up. All we have to do is remove the user’s access on DevPanel. BOOM!” - W.K.


“The easiest system to set up CI/CD with - period!” - M.O.


\* Reference names and companies disclosed after NDA.


# Key differentiators 

__Security__: 
  - Data Security. Your data never leaves your account. 
  - PII stays in YOUR account and region.


__Performance__:
  - No Noisy Neighbors. 
  - Single tenant (or your own multi-tenants) in your own account. 


__Pricing__:
  - Save up to 80%. 
  - Pay AWS directly for exactly what you use. No markups.

__Additional differentiators__:
   * DevPanel manages and supports your AWS infrastructure
   * You retain root-level control of your infrastructure and websites
   * You can extend and integrate your sites with any AWS or 3rd party tools
   * There is zero lock-in: your sites keep running even if you decide to stop using DevPanel
   * You save up to 80% on hosting - pay AWS directly without any markups
   * Flexibility to customize and optimize every aspect of your environment
   * Full stack level observability and transparency into the setup, operations and performance of your application and all components
   * Pay AWS directly for what you use - no hidden fees!
   * DevPanel helps you optimize and minimize your total AWS costs.
   * Uses best-in-class AWS-managed infrastructure 
   * Customized security setup, monitoring and alerts
   * 24x7 support for emergencies

---

DevPanel implements a highly optimized version of AWS Reference Architecture for Serverless Drupal
implementations, as described on their blog here:
[https://aws.amazon.com/blogs/storage/deploy-serverless-drupal-applications-using-aws-fargate-and-amazon-efs/](https://aws.amazon.com/blogs/storage/deploy-serverless-drupal-applications-using-aws-fargate-and-amazon-efs/)

![](Deploy-serverless-Drupal-using-AWS-Fargate-and-Amazon-EFS-architecture-diagram.png)

We start with this reference architecture and add many other components to create a highly optimized
and efficient, auto-scaling environment that grows and shrinks to accommodate any number of sites as
well as any size of site. Client sites are optimized and secured to their individual needs.


A serverless environment ensures an extremely high level of security as there are no (pet) servers
to maintain nor any security patches to apply. All containers are run on infrastructure managed by
AWS and backed up by their uptime and security SLAs.

Another benefit of a serverless environment is that it grows and shrinks based on load and demand on
the system. This ultimately translates into huge cost savings while providing a platform that easily
accommodates spikes in changes in website traffic without any manual intervention. 


After the infrastructure is set up, we work with clients to customize deployments and CI/CD
processes during implementation.


DevPanel including all of its dependencies can either be run in the client’s own account or in a
fully isolated environment with VPN access. 


For mission critical sites, we can work with AWS to set up and configure advanced DDOS protection
and monitoring. 


Cross-Account and cross-Region (CAR) Disaster Recovery (DR) options include CAR Backups and CAR
failover environments. 


Access and log monitoring and alert requirements are determined during the scoping session and
implemented as needed.


# Pricing with line-items for optional elements 


DevPanel charges platform use and support fees only. All customer sites are hosted in customer’s AWS
account and customers pay AWS directly for them. 


DevPanel’s business model is significantly different from traditional hosting companies where we do
not charge a markup on hosting fees. Since AWS fees have traditionally gone down year over year, our
customers can expect to pay fewer and fewer dollars for hosting over time. 


The additional benefit that customers get from our business model is that there’s virtually no
vendor lock-in as it applies to DevPanel. All the sites are hosted in the client's own account and
all the technologies are either AWS managed services or open-source technologies. So if and when a
client decides to stop using DevPanel, they only lose the functionality of DevPanel’s Control Panel
(Dashboard)... all their sites continue to run in their own account with full functionality. 


__Non-Recurring Fees__:


Scoping Session: One Time fee
   * Series of 2-4 hour meetings to scope implementation details
   * Output: 
     * Requirements outline, 
     * Implementation / execution plan, 
     * Implementation timeline, 
     * Cost estimates for time and materials,
     * and Team composition.


---

Implementation: One Time fee (estimated at Scoping Session)
   * Covers installation of DevPanel in user’s AWS account
   * Includes setup, configuration, customization, integrations, security, compliance checks, audit
     logs, cloud trails, monitoring & alerts, etc. 
   * Billed on a Time and Material basis
   * May last 1-3 months (depending on complexity, migrations and integrations)
   * May involve various team members, generally part-time, depending on expertise required and
     availability due to health, vacation, prior commitments, etc.

__Recurring Fees__:

Platform fees: Starts at $5k/month
   * Includes an optional “forked instance” of DevPanel on a client's AWS account
   * Covers all sites and applications in a cluster / account.
   * Includes license for unlimited sites and unlimited number of branches
   * Includes license for 5 developer (or admins)
   * Includes up to 40 hours of platform support / month 
     * Covers management, updates, upgrades, patches and support
     * Covers configuration, training, plus developer and admin support
     * Provides a managed AWS hosting platform experience
     * Additional platform support billed separately


__Support Fees__:


User Support Contracts: Sold in blocks of 40 hours
   * Covers support for all sites, apps, operations and infrastructure questions
   * For 2 named contacts (point / lead person)
   * Unused hours rolled over to next (1) month
   * Additional support is available on hourly basis
   * Support is billed against a prepaid retainer that is replenished monthly


3rd Party Support:
   * Provided by authorized partners and independent contractors
   * Partner’s Hourly and SOW rates and terms vary by project and scope


Service Level Agreement (SLA) based support may be purchased for additional fees. Support fees are
based on the combination of many factors, including but not limited to the following: Hits, Usage,
Page Views, Throughput and traffic, Metrics from Google Analytics, Response Time, Service Levels and
Size of sites.


__Custom Projects__: 
   * For consulting and integration work beyond support
   * Pricing based on separate Statements of Work (SOWs)
   * May be initiated with paid scoping sessions
   * Price estimates, milestones and timelines provided after scope is defined


# Terms & conditions


See attached sample template contracts:
   * Platform use contract
   * Consulting contract


# Client Questions:
   1. What’s today’s environment?
     - What’s working with it? What’s not? 
     - What’s getting in your way?
     - Why do you want to change it? 
     - What’s the current spend? Hard and Soft?
   2. Where are you headed? 
     - What do you want to see different?
     - Things that you’ll have to get right?...
   3. What’s your timeline?
     - Who needs to do what by when?
     - What do you want to see in the short term? By when?
     - What do you want in the long run…
   4. Who’s on the team?
     - Internal?
     - External?
     - Planned?
   5. How satisfied are you with your current…
     - Vendors?
     - Support?
     - Pricing?
   6. What do you hope to gain?
     - Outcomes? Key Results?
     - What’s desired in 30, 60, 90 days?
   7. What keeps you up at night?
     - Concens?
     - Future Plans?
   8. What else?
